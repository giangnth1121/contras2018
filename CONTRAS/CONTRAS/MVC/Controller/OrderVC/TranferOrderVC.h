//
//  TranferOrderVC.h
//  CONTRAS
//
//  Created by Giang Béo  on 3/1/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "BaseViewController.h"

@interface TranferOrderVC : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
    __weak IBOutlet UITableView *_orderTableView;
}
@property (weak, nonatomic) IBOutlet UILabel *lbTotal;
@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (nonatomic, strong)  NSMutableArray *ordersArray;

@end
