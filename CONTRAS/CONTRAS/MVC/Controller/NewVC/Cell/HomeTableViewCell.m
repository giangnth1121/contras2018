//
//  HomeTableViewCell.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "HomeTableViewCell.h"
#import "VideoModel.h"
@implementation HomeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self configUI];
    
}
- (void)configUI {
    self.durationView.layer.cornerRadius = 3.0f;
    self.durationView.layer.masksToBounds = YES;
    self.avatarImgView.layer.cornerRadius    = self.avatarImgView.frame.size.width/2;
    self.avatarImgView.layer.masksToBounds   = YES;

}
- (void)setContentCell:(VideoModel *)videoModel {
  //  self.playerView.video = videoModel;
//    author = hohongspt;
//    "author_id" = 1;
//    clip = "https://www.youtube.com/watch?v=muw7vzm__Zg";
//    comment = 34;
//    content = "<null>";
//    dislike = 83;
//    id = 644;
//    img = "http://cdn.bigduels.com/upload/fight/20171002153813.jpg";
//    like = 782;
//    share = 15;
//    "share_link" = "<null>";
//    title = "Rully Karame vs Saepul Anwar - One Pride MMA ";
//    type = 2;
    NSString *imageGIF = videoModel.img;
    NSString* encodedUrl = [imageGIF stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    [self.videoPlaceHolderImageView sd_setImageWithURL:[NSURL URLWithString:encodedUrl] placeholderImage:[UIImage imageNamed:@"ic_placeholder"]] ;
    [self.avatarImgView sd_setImageWithURL:[NSURL URLWithString:videoModel.avarta] placeholderImage:[UIImage imageNamed:@"ic_profile"]];
    self.nameLabel.text          = videoModel.title;
    self.nameLabel.textColor     = [UIColor darkTextColor];
    self.userNameLabel.text      = videoModel.author;
    self.sourceURL               = videoModel.clip;
    [self.numOfCommentsButton setTitle: [NSString stringWithFormat:@" %ld",(long)videoModel.comment] forState: UIControlStateNormal];
    self.durationLabel.text = (videoModel.duration > 0) ? [Utils formatTimeFromSeconds:videoModel.duration] : @"";

}
- (IBAction)closeTapped:(id)sender {
    if (self.delegate ) {
        [self.delegate closeVideo:self.indexRow];
    }
}
- (IBAction)commentTapped:(id)sender {
    if (self.delegate ) {
        [self.delegate commentVideo:self.indexRow];
    }
}
- (IBAction)playVideo:(id)sender {
    if (self.delegate ) {
        [self.delegate playVideo:self.indexRow];
    }
}
#pragma mark - IBAction

@end
