//
//  HomeTableViewCell.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TLVideoPlayerView.h"
#import "VideoModel.h"

@protocol HomeTableViewCellDelegate <NSObject>
- (void)closeVideo:(NSInteger)index;
- (void)commentVideo:(NSInteger)index;
- (void)playVideo:(NSInteger)index;
@end
@interface HomeTableViewCell : UITableViewCell
@property (nonatomic,strong) NSString *sourceURL;
@property (weak, nonatomic) IBOutlet UIImageView *videoPlaceHolderImageView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImgView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *numOfCommentsButton;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@property (weak, nonatomic) IBOutlet UIView *controlView;
@property (weak, nonatomic) IBOutlet UIView *durationView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (assign, nonatomic) NSInteger indexRow;
@property (weak, nonatomic) id <HomeTableViewCellDelegate> delegate;
- (void)setContentCell:(VideoModel *)videoModel ;

@end
