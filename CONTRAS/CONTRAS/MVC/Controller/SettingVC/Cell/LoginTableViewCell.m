//
//  LoginTableViewCell.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "LoginTableViewCell.h"

@implementation LoginTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.fbButton.layer.cornerRadius = self.fbButton.frame.size.width/2;
    self.fbButton.layer.masksToBounds = YES;
    self.twitterButton.layer.cornerRadius = self.twitterButton.frame.size.width/2;
    self.twitterButton.layer.masksToBounds = YES;
    
    self.googleButton.layer.cornerRadius = self.googleButton.frame.size.width/2;
    self.googleButton.layer.masksToBounds = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)loginFbTapped:(id)sender {
    if (self.delegate) {
        [self.delegate loginFacebook];
    }
}

- (IBAction)loginTwitterTapped:(id)sender {
    if (self.delegate) {
        [self.delegate loginTwitter];
    }
}
- (IBAction)loginGmailTapped:(id)sender{
    if (self.delegate) {
        [self.delegate loginGmail];
    }
}
@end
