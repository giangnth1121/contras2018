//
//  SWCollectionCell.m
//  SRPLS
//
//  Created by Tan Le on 7/26/14.
//  Copyright (c) 2014 Tan Le. All rights reserved.
//

#import "SWCollectionViewCell.h"

@interface SWCollectionViewCell () {

    UIImageView *_imageViewComment;
    
}
@end

@implementation SWCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.contentView.layer.cornerRadius = 4.0f;
        // init image view
        _imageView = [[UIImageView alloc] init];
        _imageView.contentMode = UIViewContentModeScaleAspectFill;
        _imageView.clipsToBounds = YES;
        [self.contentView addSubview:_imageView];
        
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.contentView.layer.cornerRadius = 2;
        self.contentView.clipsToBounds = YES;
        
        // init price label
        _priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 160, 130, 20)];
        _priceLabel.backgroundColor = [UIColor clearColor];
        _priceLabel.font = [UIFont boldSystemFontOfSize:14.0f];
        [self.contentView addSubview:_priceLabel];
        
        _oldPriceLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 180, 100, 20)];
        _oldPriceLabel.font = [UIFont systemFontOfSize:11.0f];
        _oldPriceLabel.textColor = [UIColor lightGrayColor];
        [self.contentView addSubview:_oldPriceLabel];
        
        // init detail label
        _detailLabel = [[UILabel alloc] initWithFrame:CGRectMake(90, 180, 100, 20)];
        _detailLabel.backgroundColor = [UIColor clearColor];
        _detailLabel.textColor = [UIColor lightGrayColor];
        _detailLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:_detailLabel];
        
        _nameUserLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 136, 100, 20)];
        _nameUserLabel.font = [UIFont systemFontOfSize:13.0f];
        _nameUserLabel.textColor = [UIColor whiteColor];
        [self.contentView addSubview: _nameUserLabel];
        
        // init label line
        UILabel *lblLine = [[UILabel alloc] initWithFrame:CGRectMake(0, 202,self.contentView.bounds.size.width,0.3)];
        lblLine.backgroundColor = [UIColor lightGrayColor];
        [self.contentView addSubview:lblLine];
        
        // init image view comment
        _imageViewComment = [[UIImageView alloc] initWithFrame:CGRectMake(30, 160, 15, 15)];
        _imageViewComment.backgroundColor = [UIColor clearColor];
        _imageViewComment.image = [UIImage imageNamed:@"Comment Pressed.png"];
        [self.contentView addSubview:_imageViewComment];
        
        // init number comment label
        _numberCommentLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 200, 100, 30)];
        _numberCommentLabel.backgroundColor = [UIColor clearColor];
        _numberCommentLabel.textColor = [UIColor lightGrayColor];
        _numberCommentLabel.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:_numberCommentLabel];
        
//        // init label line
//        UILabel *lblLine1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 283,self.contentView.bounds.size.width,1)];
//        lblLine1.backgroundColor = [UIColor lightGrayColor];
//        [self.contentView addSubview:lblLine1];

        
    }
    return self;
}

- (void) displayCommentNumber:(BOOL) flag {

    if (flag) {
        _imageViewComment.frame = CGRectMake(55, 210, 15, 15);
        _numberCommentLabel.frame = CGRectMake(77, 200, 100, 30);
    }
    else {
        _imageViewComment.frame = CGRectMake(20, 210, 15, 15);
        _numberCommentLabel.frame = CGRectMake(42, 200, 100, 30);
    }
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

- (void)layoutSubviews {

    _imageView.frame = CGRectMake(0, 0, 150, 160);
}

@end
