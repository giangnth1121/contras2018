//
//  BaseModel.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "BaseModel.h"

@implementation BaseModel

+(NSArray *)parseWithListDictionary:(NSArray *)list
{
    NSMutableArray *results = [[NSMutableArray alloc] init];
    
    for (NSDictionary *item in list)
    {
        BaseModel *obj = [[self class] parseWithDictionary:item];
        
        [results addObject:obj];
    }
    
    return results;
}

+(instancetype)parseWithDictionary:(NSDictionary *)dict
{
    @throw @"Missing parse with dictionary function";
}

@end
