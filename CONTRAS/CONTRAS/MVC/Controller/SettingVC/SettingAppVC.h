//
//  SettingAppVC.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingAppVC : BaseViewController

@property (nonatomic) IBOutlet UITableView *settingAppTableView;
@end
