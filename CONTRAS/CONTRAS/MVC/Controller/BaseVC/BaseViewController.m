//
//  BaseViewController.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "BaseViewController.h"

#define debug 0
@interface BaseViewController ()

@end

@implementation BaseViewController
{
    BOOL isWillFirstAppeared;
    BOOL isDidFirstAppeared;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self config];
}
- (BOOL)prefersStatusBarHidden {
    return YES;
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!isWillFirstAppeared)
    {
        isWillFirstAppeared = YES;
        
        [self viewWillFirstAppear];
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if (!isDidFirstAppeared)
    {
        isDidFirstAppeared = YES;
        
        [self viewDidFirstAppear];
    }
}

-(void)viewWillFirstAppear
{
    
}

-(void)viewDidFirstAppear
{
    
}

#pragma mark - Config

-(void) config
{
    
}
- (void)setRightButtonBarWithImage:(NSString *)image action:(SEL)selector  {
    [self.navigationController setRightNavigationButtonImage:image action:selector];
}
 
- (void)setBackNavigationButton{
    [self.navigationController setLeftNavigationButtonImage:@"ic_back" action:@selector(backTapped)];
}

- (void)backTapped {
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - LoadData

#pragma mark - IBAction

#pragma mark - Delegate

#pragma mark - Gesture

#pragma mark - Utils
#pragma mark - Alert
-(void)showAlertWithMessage:(NSString *)message
{
    [self showAlertWithTitle:@"" message:message buttons:nil action:nil];
}

-(void)showErrorAlertWithMessage:(NSString *)message
{
    [self showAlertWithTitle:@"Error" message:message buttons:nil action:nil];
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message
{
    [self showAlertWithTitle:title message:message buttons:nil action:nil];
}

-(void)showAlertWithMessage:(NSString *)message action:(void (^)(NSInteger))actionHandler
{
    [self showAlertWithTitle:@"" message:message buttons:nil action:actionHandler];
}

-(void)showAlertWithMessage:(NSString *)message buttons:(NSArray *)buttons action:(void (^)(NSInteger))actionHandler
{
    [self showAlertWithTitle:@"" message:message buttons:buttons action:actionHandler];
}

-(void)showAlertWithMessage:(NSString *)message buttons:(NSArray *)buttons destructiveIndexs:(NSArray<NSNumber *> *)destructiveIndexs action:(void (^)(NSInteger))actionHandler
{
    [self showAlertWithTitle:@"" message:message buttons:buttons destructiveIndex:destructiveIndexs action:actionHandler];
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons action:(void (^)(NSInteger))actionHandler
{
    [self showAlertWithTitle:title message:message buttons:buttons destructiveIndex:nil action:actionHandler];
}

-(void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons destructiveIndex:(NSArray<NSNumber *> *)destructiveIndexs action:(void (^)(NSInteger))actionHandler
{
    
    if (!buttons)
    {
        buttons = @[@"OK"];
    }
    
    if (!title)
    {
        title = @"";
    }
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (NSString *buttonString in buttons)
    {
        UIAlertActionStyle style = UIAlertActionStyleDefault;
        if ([destructiveIndexs containsObject:@([buttons indexOfObject:buttonString])])
        {
            style = UIAlertActionStyleDestructive;
        }
        
        UIAlertAction *alertAction = [UIAlertAction actionWithTitle:buttonString style:style handler:^(UIAlertAction * _Nonnull action) {
            if (action)
            {
                if (actionHandler)
                {
                    actionHandler([buttons indexOfObject:buttonString]);
                }
            }
        }];
        
        [alert addAction:alertAction];
    }
    
    [self presentViewController:alert animated:YES completion:nil];
}


@end

