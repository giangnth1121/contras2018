//
//  BaseListVC.h
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseListVC : BaseViewController

@property (assign, nonatomic) listType listType;
@end
