//
//  SegmentView.m
//  Assignment
//
//  Created by Nguyen Ha Giang on 9/15/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "SegmentView.h"

@implementation SegmentView
{
    NSMutableArray<UIView *> *segmentViews;
}
-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    return self;
}

-(void)setSelectedColor:(UIColor *)selectedColor
{
    _selectedColor = selectedColor;
    
    self.backgroundColor = selectedColor;
}

-(void)setSegments:(NSArray<NSString *> *)segments
{
    _segments = segments;
    
    segmentViews = [[NSMutableArray alloc] init];
    UIView *firstSegment = [segmentViews firstObject];
    UIView *previousSegment;
    for(int i = 0; i < segments.count; i++)
    {
        NSString *segmentTitle = [segments objectAtIndex:i];
        UIView *segmentView = [[UIView alloc] initWithFrame:CGRectZero];
        segmentView.translatesAutoresizingMaskIntoConstraints = NO;
        
        [self addSubview:segmentView];
        
        UIButton *segmentButton = [[UIButton alloc] initWithFrame:CGRectZero];
        segmentButton.tag = i;
        [segmentButton addTarget:self action:@selector(segmentButtonDidTap:) forControlEvents:UIControlEventTouchUpInside];
        [segmentButton setTitle:segmentTitle forState:UIControlStateNormal];
        [segmentButton setTitleColor:self.titleNormalColor forState:UIControlStateNormal];
        [segmentView addSubview:segmentButton];
        [self addConstraintFitView:segmentView view:segmentButton];
        
        if (i==0)
        {
            firstSegment = segmentView;
            [self addConstraint:[NSLayoutConstraint constraintWithItem:segmentView attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeLeading multiplier:1 constant:2]];
        }
        else
        {
             [self addConstraint:[NSLayoutConstraint constraintWithItem:segmentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:firstSegment attribute:NSLayoutAttributeWidth multiplier:1 constant:0]];
        }
        
        if (i==segments.count - 1)
        {
            [self addConstraint:[NSLayoutConstraint constraintWithItem:segmentView attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1 constant:-2]];
        }
        
        if (previousSegment)
        {
            [self addConstraint:[NSLayoutConstraint constraintWithItem:segmentView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:previousSegment attribute:NSLayoutAttributeRight multiplier:1 constant:1]];
        }
        
        [self addConstraint:[NSLayoutConstraint constraintWithItem:segmentView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1 constant:2]];
        [self addConstraint:[NSLayoutConstraint constraintWithItem:segmentView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeBottom multiplier:1 constant:-2]];
        
        previousSegment = segmentView;
        [segmentViews addObject:segmentView];
    }
    
    [self refreshSegmentUIWithIndex:0];
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    [self setupUISegment];
}

-(void) setupUISegment
{
    UIView *firstSegment = [segmentViews firstObject];
    UIView *lastSegment = [segmentViews lastObject];
    
    UIBezierPath *firstItemmaskPath = [UIBezierPath bezierPathWithRoundedRect:firstSegment.bounds byRoundingCorners:( UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(18, 18)];
    CAShapeLayer *firstItemMaskLayer = [[CAShapeLayer alloc] init];
    firstItemMaskLayer.frame = lastSegment.bounds;
    firstItemMaskLayer.path  = firstItemmaskPath.CGPath;
    firstSegment.layer.mask = firstItemMaskLayer;
    firstSegment.layer.masksToBounds = YES;
    
    UIBezierPath *lastItemmaskPath = [UIBezierPath bezierPathWithRoundedRect:lastSegment.bounds byRoundingCorners:( UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(18, 18)];
    CAShapeLayer *lastItemMaskLayer = [[CAShapeLayer alloc] init];
    lastItemMaskLayer.frame = lastSegment.bounds;
    lastItemMaskLayer.path  = lastItemmaskPath.CGPath;
    lastSegment.layer.mask = lastItemMaskLayer;
    lastSegment.layer.masksToBounds = YES;
}

-(void) refreshSegmentUIWithIndex:(NSInteger)index
{
    for (UIView *segment in segmentViews)
    {
        segment.backgroundColor = self.normalColor;
        UIButton *segmentButton = (UIButton *)[segment.subviews firstObject];
        [segmentButton setTitleColor:self.titleNormalColor forState:UIControlStateNormal];
        
        [segment setNeedsDisplay];
        [segmentButton setNeedsDisplay];
    }
    
    UIView *selectedSegment = [segmentViews objectAtIndex:index];
    selectedSegment.backgroundColor = self.selectedColor;
    UIButton *selectedSegmentButton = (UIButton *)[selectedSegment.subviews firstObject];
    [selectedSegmentButton setTitleColor:self.titleSelectedColor forState:UIControlStateNormal];
}

-(void) addConstraintFitView:(UIView *)superView view:(UIView *)view
{
    view.translatesAutoresizingMaskIntoConstraints = NO;
    
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeLeading relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeLeading multiplier:1 constant:0]];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeTrailing multiplier:1 constant:0]];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeTop multiplier:1 constant:0]];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:view attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0]];
}

-(void) segmentButtonDidTap:(UIButton *)sender
{
    [self refreshSegmentUIWithIndex:sender.tag];
    self.selectedIndex = sender.tag;
    
    if ([self.delegate respondsToSelector:@selector(segmentView:didSelectAtIndex:)])
    {
        [self.delegate segmentView:self didSelectAtIndex:sender.tag];
    }
}

@end
