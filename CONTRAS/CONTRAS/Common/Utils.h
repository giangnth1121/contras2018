//
//  Utils.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/13/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppDelegate.h"

@interface Utils : NSObject
NSString *OLang(NSString *key);

+ (NSString *)getUUIDDevice;
+ (NSString *)formatCurrency :(float)value;
#pragma mark - Helper
+(NSString *) getTimeStringFromTime:(NSInteger)time;
+(NSString *)formatTimeFromSeconds:(int)numberOfSeconds;
@end
