//
//  TypeCollectionReusableView.h
//  SACH
//
//  Created by Giang Béo  on 12/28/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TypeCollectionReusableView : UICollectionReusableView
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end
