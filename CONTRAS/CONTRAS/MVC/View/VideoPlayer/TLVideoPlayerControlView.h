//
//  TLVideoPlayerControlView.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIView+Utils.h"

typedef enum : NSUInteger
{
    ControlStateIdle = 0,
    ControlStateLoading = 1,
    ControlStatePlaying = 2,
    ControlStateShowSeekControl = 3,
    ControlStatePause = 4,
    ControlStateLoadingBuffer = 5,
} ControlState;

@protocol TLVideoPlayerControlViewDelegate;
@interface TLVideoPlayerControlView : UIView
@property (weak, nonatomic) id<TLVideoPlayerControlViewDelegate> delegate;
@property (nonatomic,strong) NSString *videoTitle;
@property (nonatomic) NSInteger videoDuration;
@property (nonatomic,strong) NSString *thumbnailURL;
@property (nonatomic) ControlState controlState;
//Outlet
@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;
@property (weak, nonatomic) IBOutlet UIView *seekControlContainerView;
@property (weak, nonatomic) IBOutlet UIView *videoTimeContainerView;
@property (weak, nonatomic) IBOutlet UIView *playButtonContainerView;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *waitActivityView;

@property (weak, nonatomic) IBOutlet UILabel *currentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationTimeLabel;
@property (weak, nonatomic) IBOutlet UISlider *timeSlider;

@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UIButton *fbButton;
@property (weak, nonatomic) IBOutlet UIButton *twButton;
@property (weak, nonatomic) IBOutlet UIButton *messengerButton;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIButton *replayButton;
- (IBAction)playButtonDidTap:(UIButton *)sender;
- (IBAction)fullscreenButtonDidTap:(id)sender;
- (IBAction)sliderDidChangeValue:(UISlider *)sender;

// Public function
-(void) resetState;
@end

@protocol TLVideoPlayerControlViewDelegate <NSObject>
-(void) TLVideoPlayerControlViewDidTapPlay:(TLVideoPlayerControlView *)controller;
-(void) TLVideoPlayerControlView:(TLVideoPlayerControlView *)controller didSeekToValue:(NSInteger)value;
-(void) TLVideoPlayerControlViewDidTapPause:(TLVideoPlayerControlView *)controller;
-(void) TLVideoPlayerControlViewDidTapFullscreen:(TLVideoPlayerControlView *)controller;

-(void) videoShareFb:(TLVideoPlayerControlView *)controller;
-(void) videoShareTw:(TLVideoPlayerControlView *)controller;
-(void) videoShareMessenger:(TLVideoPlayerControlView *)controller;
-(void) videoShareMore:(TLVideoPlayerControlView *)controller;
-(void) videoReplay:(TLVideoPlayerControlView *)controller;

@end
