//
//  HomeVC.m
//  SACH
//
//  Created by Giang Béo  on 12/25/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "HomeVC.h"
#import "CHTCollectionViewWaterfallLayout.h"
#import "HomeCollectionViewCell.h"
#import "Macro.h"
#import "TypeCollectionReusableView.h"
#import "DetailVC.h"

@interface HomeVC () <UICollectionViewDelegate, UICollectionViewDataSource,CHTCollectionViewDelegateWaterfallLayout>
{
    NSArray *sortedKeys;
    NSString *key;
    NSArray *typeSourceArrays;
    NSDictionary *sourceDict;
}

@property (nonatomic, strong)  UICollectionView *collectionView;


@end
@implementation HomeVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self initData];
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    self.title = @"SẠCH";
    CHTCollectionViewWaterfallLayout *layout = [[CHTCollectionViewWaterfallLayout alloc] init];
    layout.minimumColumnSpacing = 10;
    layout.minimumInteritemSpacing = 10;
    
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH_PORTRAIT, SCREEN_HEIGHT_PORTRAIT) collectionViewLayout:layout];
    
    self.collectionView .dataSource = self;
    self.collectionView .delegate = self;
    // self.collectionView .backgroundColor = [SWUtil getColor:@"F6F6F6"];
    self.collectionView .backgroundColor = [UIColor whiteColor];
    [ self.collectionView  registerClass:[HomeCollectionViewCell class]
              forCellWithReuseIdentifier:@"HomeCollectionViewCell"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"HomeCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"HomeCollectionViewCell"];
    [self.collectionView registerClass:[TypeCollectionReusableView class]
           forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader
                  withReuseIdentifier:@"headerView"];
    [self.collectionView registerNib:[UINib nibWithNibName:@"TypeCollectionReusableView" bundle:nil] forSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"headerView"];
    
    
    [self.view addSubview:self.collectionView];
}

- (void)initData {
    
    // read file Setting.plist
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"Sources" ofType:@"plist"];
    sourceDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)setSectionData:(NSInteger)section
{
    sortedKeys = [[sourceDict allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    key = [sortedKeys objectAtIndex:section];
    typeSourceArrays = [sourceDict objectForKey:key];
    
}
#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    
    return  [[sourceDict allKeys] count];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {

    // set Data for section
    [self setSectionData:section];
    
    return [typeSourceArrays count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    HomeCollectionViewCell *cell = (HomeCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCollectionViewCell" forIndexPath:indexPath];
   // cell.backgroundColor = [UIColor blueColor];
    [self setSectionData:indexPath.section];
    
    NSDictionary *dict = [typeSourceArrays objectAtIndex:indexPath.row];
    [cell setContentCell:dict];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self setSectionData:indexPath.section];
    DetailVC *detailVC = [[DetailVC alloc] init];
    detailVC.infoDict =  [typeSourceArrays objectAtIndex:indexPath.row];;
    [self.navigationController pushViewController:detailVC animated:YES];
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    UICollectionReusableView *reusableview = nil;
    
    if (kind == CHTCollectionElementKindSectionHeader) {
        
        TypeCollectionReusableView *headerView = [collectionView dequeueReusableSupplementaryViewOfKind:CHTCollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        [self setSectionData:indexPath.section];
        headerView.titleLabel.text = [key uppercaseString];
        
        reusableview = headerView;
    }
    
    return reusableview;
}
- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(SCREEN_WIDTH_PORTRAIT/2-20, 200);
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout heightForHeaderInSection:(NSInteger)section {
    return 40;
}
@end
