//
//  TranferOrderVC.m
//  CONTRAS
//
//  Created by Giang Béo  on 3/1/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "TranferOrderVC.h"
#import "ProductCell.h"
#import "TranferOrderCell.h"
#import "PaymentCell.h"

@interface TranferOrderVC ()
{
    CGFloat _totalPrice;
}
@end

@implementation TranferOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self getTotalPrice];
    // Do any additional setup after loading the view from its nib.
}

- (void)configUI {
    
    self.title = @"XÁC NHẬN ĐƠN HÀNG";
    [self setBackNavigationButton];
    _orderTableView.dataSource      = self;
    _orderTableView.delegate        = self;
    _orderTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_orderTableView registerNib:[UINib nibWithNibName:@"ProductCell" bundle:nil] forCellReuseIdentifier:@"ProductCell"];
    [_orderTableView registerNib:[UINib nibWithNibName:@"TranferOrderCell" bundle:nil] forCellReuseIdentifier:@"TranferOrderCell"];
    [_orderTableView registerNib:[UINib nibWithNibName:@"PaymentCell" bundle:nil] forCellReuseIdentifier:@"PaymentCell"];
    
    self.btnConfirm.layer.cornerRadius = 5.0f;
    self.btnConfirm.layer.masksToBounds = YES;
    self.btnConfirm.backgroundColor = [UIColor purpleColor];
    self.lbTotal.font = [UIFont boldSystemFontOfSize:17];
    self.lbTotal.textColor = [UIColor purpleColor];
}
- (void)getTotalPrice {
    NSInteger total = 0;
    for (int i = 0; i < [self.ordersArray count]; i ++) {
        NSDictionary *dict = self.ordersArray[i];
        total += [dict[@"price"] integerValue]* [dict[@"number"] integerValue] ;
    }
    _totalPrice = total;
    self.lbTotal.text = [NSString stringWithFormat:@"%@",[Utils formatCurrency:total] ];
    [_orderTableView reloadData];
  //  self.totalCoinsLabel.text = [NSString stringWithFormat:@"Nhận %ld xu",total/30000];
}
- (IBAction)confirmOrderTapped:(id)sender {
    
    NSString *title = [NSString stringWithFormat:@"TỔNG : %@",[Utils formatCurrency:_totalPrice] ];;
    for (int i = 0 ; i <self.ordersArray.count; i ++) {
        NSDictionary *dict = self.ordersArray[i];
      //  NSInteger price = [dict[@"price"] integerValue];
        NSInteger number = [dict[@"number"] integerValue];
        title = [title stringByAppendingString:[NSString stringWithFormat:@"\n%ld x %@",number, dict[@"title"]]];

    }
    [[Global share] showAlertWithTitle:@"XÁC NHẬN ĐƠN HÀNG"
                               message:title
                           titleButton:@"OK"
                                action:^{
                                    
                                } inView:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView Datasource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  (section == 0) ? [self.ordersArray count] : 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        ProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
        [cell setContentOrderCell:self.ordersArray[indexPath.row]];
        return cell;
    } else  if (indexPath.section == 1)  {
        if (_totalPrice >250000) {
            PaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
            cell.lbTitle.text = @"Miễn phí vận chuyển";
            return cell;
        } else {
            TranferOrderCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TranferOrderCell" forIndexPath:indexPath];
            return cell;
        }
     
    } else {
        PaymentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"PaymentCell" forIndexPath:indexPath];
        cell.lbTitle.text = @"Thanh toán khi nhận hàng";
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0) ? 80  : (indexPath.section == 1) ? ( (_totalPrice >250000) ?44 :130) : 44;
}
@end
