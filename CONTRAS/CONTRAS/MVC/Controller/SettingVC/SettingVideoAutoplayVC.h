//
//  SettingVideoAutoplayVC.h
//  TopShare
//
//  Created by Giang Béo  on 11/30/17.
//  Copyright © 2017 Retech. All rights reserved.
//

#import "BaseViewController.h"

@interface SettingVideoAutoplayVC : BaseViewController
@property (nonatomic) IBOutlet UITableView *settingAppTableView;
@end
