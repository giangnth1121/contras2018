//
//  BaseViewController.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
@property (nonatomic) BOOL isAvailableConnection;
#pragma mark - Life time
-(void) viewWillFirstAppear;
-(void) viewDidFirstAppear;

#pragma mark - Utils
#pragma mark - Config
- (void)setRightButtonBarWithImage:(NSString *)image action:(SEL)selector  ;
- (void)setBackNavigationButton;
#pragma mark - Alert
-(void) showAlertWithMessage:(NSString *)message;
-(void) showErrorAlertWithMessage:(NSString *)message;
-(void) showAlertWithTitle:(NSString *)title message:(NSString *)message;
-(void) showAlertWithMessage:(NSString *)message action:(void (^)(NSInteger buttonIndex))actionHandler;
-(void) showAlertWithMessage:(NSString *)message buttons:(NSArray *)buttons action:(void (^)(NSInteger buttonIndex))actionHandler;
-(void) showAlertWithMessage:(NSString *)message buttons:(NSArray *)buttons destructiveIndexs:(NSArray<NSNumber *> *)destructiveIndexs action:(void (^)(NSInteger buttonIndex))actionHandler;
-(void) showAlertWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons action:(void (^)(NSInteger buttonIndex))actionHandler;
-(void) showAlertWithTitle:(NSString *)title message:(NSString *)message buttons:(NSArray *)buttons destructiveIndex:(NSArray<NSNumber *> *)destructiveIndexs action:(void (^)(NSInteger buttonIndex))actionHandler;
@end

