//
//  OrderSubmitCell.h
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OrderSubmitCellDelegate <NSObject>
@optional
- (void)updateOrderInfo:(NSDictionary*)info idx:(NSInteger)idx;
- (void)checkRemoveOrder:(NSDictionary *)info isRemove:(BOOL)isRemove idx:(NSInteger)idx;
@end
@interface OrderSubmitCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *iconImg;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *localizePriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;
@property (nonatomic, weak) IBOutlet UIButton *increaseButton;
@property (nonatomic, weak) IBOutlet UIButton *reduceButton;
@property (nonatomic, weak) IBOutlet UIButton *numberOfOrderButton;
@property (nonatomic, weak) IBOutlet UIButton *checkButton;

@property (nonatomic) NSInteger idxRow;
@property (nonatomic) NSDictionary *infoFruit;
@property (nonatomic) NSInteger price;
@property (nonatomic, weak) id <OrderSubmitCellDelegate> delegate;
- (void)setContentOrderCell:(NSDictionary *)dict;
- (void)setSelectedButton:(BOOL)isSelected;

@end
