//
//  ManageAccountVC.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "ManageAccountVC.h"
#import "ManageAccountCell.h"
#import "SettingVC.h"
#import "AppDelegate.h"


@interface ManageAccountVC () <UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate , ManageAccountCellDelegate>
{
    NSMutableArray *_title1Array;
    NSMutableArray *_title2Array;
    UIImage *_avatarImage;
}
@property (nonatomic, strong) IBOutlet UITableView *settingTableView;
@end

@implementation ManageAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)configUI{
    self.title = @"Account Management";
    [self setBackNavigationButton];
    self.settingTableView.dataSource = self;
    self.settingTableView.delegate = self;
    [self.settingTableView registerNib:[UINib nibWithNibName:@"ManageAccountCell" bundle:nil] forCellReuseIdentifier:@"ManageAccountCell"];
    self.settingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _title1Array     = [@[@"Profile Picture",
                         @"Name",
                         @"E-mail"] mutableCopy];
    
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USERNAME];
    NSString *nameFB = ([[Global share] isHasAccountFB]) ? [NSString stringWithFormat:@"Facebook (%@)",username ] : @"Facebook";
    NSString *nameTwitter = ([[Global share] isHasAccountTwitter]) ? [NSString stringWithFormat:@"Twitter (%@)",username ] : @"Twitter";
    NSString *nameGoogle = ([[Global share] isHasAccountGoogle]) ? [NSString stringWithFormat:@"Google (%@)",username ] : @"Google";
    
    _title2Array    = [@[ nameFB,
                          nameTwitter,
                          nameGoogle] mutableCopy];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
    
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return (section == 2) ? 1 : [_title1Array count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ManageAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ManageAccountCell"];
    cell.delegate    = self;
    cell.indexRow    = indexPath.row;
    [cell resetCell];
    switch (indexPath.section) {
        case 0:
        {
            cell.titleLabel.text = _title1Array[indexPath.row];
            [cell.avaImg setHidden:(indexPath.row != 0)];
            NSString *linkAvatar = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LINKAVATAR];
            if (_avatarImage)  {
                [cell.avaImg setImage:_avatarImage];
            } else {
                [cell.avaImg sd_setImageWithURL:[NSURL URLWithString:linkAvatar] placeholderImage:[UIImage imageNamed:@"ic_profile"]];
            }
            
            cell.detailLabel.text = (indexPath.row == 1) ? [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USERNAME]:@"";
            [cell.arrowImg setHidden:(indexPath.row == 2)];
        }
            break;
        case 1:
            cell.titleLabel.text = _title2Array[indexPath.row];
          
            switch (indexPath.row) {
                case 0:
                    [cell.switchButton setOn:[[Global share] isHasAccountFB]];
                    break;
                case 1:
                    [cell.switchButton setOn:[[Global share] isHasAccountTwitter]];
                    break;
                case 2:
                    [cell.switchButton setOn:[[Global share] isHasAccountGoogle]];
                    break;
                default:
                    break;
            }
            [cell.switchButton setHidden:NO];
            
            
            break;
        case 2:
            cell.centerLabel.text = @"Logout";
            break;
        default:
            break;
    }
    return cell;
}
#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            if (indexPath.section == 0) {
                [[Global share] showActionSheetWithTitle:nil
                                                    inView:self
                                                    title1:@"Take Photo"
                                                   action1:^{
                                                       [self takePhoto];
                                                   } title2:@"Choose from gallery"
                                                   action2:^{
                                                       [self chooseLibrary];
                                                   }];
                
            }
            break;
        case 1:
            
            break;
        case 2:
        {
            [[Global share] showAlertWithMessage:@"Are you sure logout?"
                                     titleButton:@"Logout"
                                          action:^{
                
                [self resetUser];
               [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLogout object:nil];
                [self.navigationController popViewControllerAnimated:YES];
            } inView:self];
        }
            break;
        default:
            break;
    }
    [self.settingTableView reloadData];
}
- (void)takePhoto {
    @try
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        picker.delegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    @catch (NSException *exception)
    {
        [[Global share] showMessage:@"CameraUnvailable"
                               inView:self];
    }
}
- (void)chooseLibrary {
    @try
    {
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.sourceType   = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.delegate     = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    }
    @catch (NSException *exception)
    {
        [[Global share] showMessage:@"CameraUnvailable"
                               inView:self];
    }
}

#pragma mark - Image picker delegate

- (void) imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    UIImage *imageSelected = [ info objectForKey:UIImagePickerControllerOriginalImage];;
    NSData *data  = UIImageJPEGRepresentation(imageSelected, 0.7);
    _avatarImage = imageSelected;
    [self.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void) imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [self dismissViewControllerAnimated:YES completion:NULL];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return (indexPath.section == 0 && indexPath.row == 0) ? 80 : 45;
}

#pragma mark -  ManageAccountCellDelegate

- (void)enableLoginSocialAccount:(NSInteger)index {
    switch (index) {
        case 0:// Facebook
        {
            if ([[Global share] isHasAccountFB]) {
                [[Global share] showAlertDisconnectSocialWithMessage:@"Facebook"
                                                              action:^{
                    
                } inView:self];
            } else {
               
            }
        }
            break;
        case 1:// Twitter
        {
            if ([[Global share] isHasAccountTwitter]) {
                [[Global share] showAlertDisconnectSocialWithMessage:@"Twitter"
                                                              action:^{
                                                                  
                                                              } inView:self];
            } else {
                
            }
        }
            break;
        case 2:// Google
        {
            if ([[Global share] isHasAccountGoogle]) {
                [[Global share] showAlertDisconnectSocialWithMessage:@"Google"
                                                              action:^{
                                                                  
                                                              } inView:self];
            } else {
                
            }
        }
            break;
        default:
            break;
    }
}
- (void)resetUser{
    
    [AppDelegate shareAppDelegate].loginType = kLogout;
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:IS_LOGINED];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KEY_USER_ID];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KEY_LINKAVATAR];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:KEY_USERNAME];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
@end

