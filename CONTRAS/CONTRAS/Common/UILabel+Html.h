//
//  UILabel+Html.h
//  TGS
//
//  Created by Nguyen Ha Giang on 3/2/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (Html)
- (void) setHtml: (NSString*) html;
@end
