//
//  BaseListVC.m
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "BaseListVC.h"

@interface BaseListVC () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *listTableView;
@end

@implementation BaseListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    NSString *title = @"";
    switch (self.listType) {
        case cityType:
            title = @"Tỉnh/Thành phố";
            break;
        case districtType:
            title = @"Quận/Huyện";
            break;
        case streetType:
            title = @"Phường/Xã";
            break;
        default:
            break;
    }
    self.title = title;
    self.listTableView.dataSource = self;
    self.listTableView.delegate = self;
    [self.listTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    self.listTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self setBackNavigationButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 10;// [_addressArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = @"Hà Nội";
    return cell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}
@end
