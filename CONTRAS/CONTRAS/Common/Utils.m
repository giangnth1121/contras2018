//
//  Utils.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/13/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "Utils.h"
#import <CommonCrypto/CommonDigest.h>
#import <AudioToolbox/AudioToolbox.h>
#define     FORMAT_DATE                         @"YYYY-MM-dd HH:mm:ss"
#define UNIT_SECOND     @"giây"
#define UNIT_MINUTES    @"phút"
#define UNIT_HOUR       @"giờ"
#define UNIT_DAY        @"ngày"
#define UNIT_WEEK       @"tuần"
#define UNIT_MONTH      @"tháng"
#define UNIT_YEAR       @"năm"
@implementation Utils
NSString *OLang(NSString *key)
{
    return NSLocalizedString(key, key);
}

#pragma mark - UIID Device

+ (NSString *)getUUIDDevice {
    return  [[[UIDevice currentDevice] identifierForVendor] UUIDString] ? [[[UIDevice currentDevice] identifierForVendor] UUIDString]:@"";
}

+ (NSString *)formatCurrency :(float)value {
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    return [NSString stringWithFormat:@"%@đ",[formatter stringFromNumber:[NSNumber numberWithFloat:value]]];
}
#pragma mark - Helper
+(NSString *) getTimeStringFromTime:(NSInteger)time
{
    if (time == 0) return  @"";
    NSInteger hour = time/3600;
    NSInteger minute = (time - hour*3600)/60;
    NSInteger second = (NSInteger)time % 60;
    
    if (hour != 0)
    {
        return [NSString stringWithFormat:@"%02d:%02d:%02d",(int)hour,(int)minute,(int)second];
    }
    else
    {
        return [NSString stringWithFormat:@"%02d:%02d",(int)minute,(int)second];
    }
}
+(NSString *)formatTimeFromSeconds:(int)numberOfSeconds
{
    int seconds = numberOfSeconds % 60;
    int minutes = (numberOfSeconds / 60) % 60;
    int hours = numberOfSeconds / 3600;
    
    //we have >=1 hour => example : 3h:25m
    if (hours) {
        return [NSString stringWithFormat:@"%d:%02d:%02d", hours, minutes,seconds];
    }
    //we have 0 hours and >=1 minutes => example : 3m:25s
    if (minutes) {
        return [NSString stringWithFormat:@"%d:%02d", minutes, seconds];
    }
    //we have only seconds example : 25s
    return [NSString stringWithFormat:@"00:%d", seconds];
}
@end
