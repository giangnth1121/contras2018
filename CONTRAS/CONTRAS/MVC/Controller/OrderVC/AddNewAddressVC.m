//
//  AddNewAddressVC.m
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//
//– Company’s name: Tên tổ chức (nếu có)
//– Building, apartment / flat: Nhà, căn hộ
//– Alley: ngách
//– Lane: ngõ
//– Hamlet: Thôn
//– Village: Xã
//– Street: Đường
//– Sub-district or block or neighborhood: phường
//– District or Town: huyện
//– Province: tỉnh
//– City: Thành phố
//– State: tiểu bang
#import "AddNewAddressVC.h"
#import "BaseListVC.h"

@interface AddNewAddressVC ()
{
    
}
@property (weak, nonatomic) IBOutlet UITextField *userNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *phoneTextField;
@property (weak, nonatomic) IBOutlet UITextField *cityTextField;
@property (weak, nonatomic) IBOutlet UITextField *districtTextField;
@property (weak, nonatomic) IBOutlet UITextField *subDistrictTextField;
@property (weak, nonatomic) IBOutlet UITextField *addressTextField;
@property (weak, nonatomic) IBOutlet UISwitch *switchButton;
@end

@implementation AddNewAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    self.title = @"ĐỊA CHỈ MỚI";
    [self setBackNavigationButton];
    self.btnComplete.layer.cornerRadius = 5.0f;
    self.btnComplete.layer.masksToBounds = YES;
    self.btnComplete.backgroundColor = [UIColor purpleColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction
- (IBAction)chooseCityTapped:(id)sender {
    [self pushListWithType:cityType];
}
- (IBAction)chooseDistrictTapped:(id)sender {
    [self pushListWithType:districtType];
}
- (IBAction)chooseSubDistrictTapped:(id)sender {
    [self pushListWithType:streetType];
}
- (void)pushListWithType:(listType) listType {
    BaseListVC *listVC = [[BaseListVC alloc] initWithNibName:@"BaseListVC" bundle:nil];
    listVC.listType = listType;
    [self.navigationController pushViewController:listVC animated:YES];
}
- (IBAction)addAddressTapped:(id)sender {
    NSString *username      = stringWithoutSpace(self.userNameTextField.text);
    NSString *phone         = stringWithoutSpace(self.phoneTextField.text);
    NSString *city           = stringWithoutSpace(self.cityTextField.text);
    NSString *district       = stringWithoutSpace(self.districtTextField.text);
    NSString *subDistrict    = stringWithoutSpace(self.subDistrictTextField.text);
    NSString *address        = stringWithoutSpace(self.addressTextField.text);
    
    if (StringIsNullOrEmpty(username)) [[Global share] showMessage:@"Invalid Username" inView:self]; return;
    if (StringIsNullOrEmpty(phone)) [[Global share] showMessage:@"Invalid Phone" inView:self]; return;
    if (StringIsNullOrEmpty(city)) [[Global share] showMessage:@"Invalid City" inView:self]; return;
    if (StringIsNullOrEmpty(district)) [[Global share] showMessage:@"Invalid District" inView:self]; return;
    if (StringIsNullOrEmpty(subDistrict)) [[Global share] showMessage:@"Invalid District" inView:self]; return;
    if (StringIsNullOrEmpty(address)) [[Global share] showMessage:@"Invalid Address" inView:self]; return;
}

@end
