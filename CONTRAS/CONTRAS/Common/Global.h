//
//  Global.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <Foundation/Foundation.h>


static inline NSString *stringWithoutSpace(NSString *str)
{
    NSString *result;
    if (str &&  ![str isEqual:[NSNull null]]) {
        result = [str stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    } else {
        result = @"";
    }
    
    return result?:@"";
}

@interface Global : NSObject
+(Global*)share;

- (void) showMessage:(NSString *)message inView:(id)mySelf;

- (void) showActionSheetWithTitle:(NSString *)title inView:(id) mySelf title1:(NSString *)title1  action1:(void (^) (void))handleAction1 title2:(NSString *)title2 action2:(void (^) (void))handleAction2;

- (void)showAlertDisconnectSocialWithMessage:(NSString *) message
                                      action:(void (^) (void))handleAction
                                      inView:(id)mySelf;

- (void)showAlertWithMessage:(NSString *) message
                titleButton :(NSString *)titleButton
                      action:(void (^) (void))handleAction
                      inView:(id)mySelf;

- (void)showAlertWithTitle:(NSString *) title message:(NSString *)message  titleButton :(NSString *)titleButton action:(void (^) (void))handleAction  inView:(id)mySelf;

- (BOOL)isHasAccountFB;
- (BOOL)isHasAccountTwitter;
- (BOOL)isHasAccountGoogle ;


@end
