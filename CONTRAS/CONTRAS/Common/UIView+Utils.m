//
//  UIView+Utils.m
//  TGS
//
//  Created by Nguyen Ha Giang on 2/27/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "UIView+Utils.h"

@implementation UIView (Utils)
- (NSMutableArray*)allSubViews
{
    NSMutableArray *arr=[[NSMutableArray alloc] init];
    [arr addObject:self];
    for (UIView *subview in self.subviews)
    {
        [arr addObjectsFromArray:(NSArray*)[subview allSubViews]];
    }
    return arr;
}
+(UIView *)loadViewWithNib
{
    NSString *className = NSStringFromClass(self);
    
    return [[NSBundle mainBundle] loadNibNamed:className owner:nil options:nil].lastObject;
}
@end
