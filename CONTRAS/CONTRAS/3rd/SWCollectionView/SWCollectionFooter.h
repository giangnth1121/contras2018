//
//  SWCollectionFooter.h
//  SRPLS
//
//  Created by Tan Le on 7/26/14.
//  Copyright (c) 2014 Tan Le. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWCollectionFooter : UICollectionReusableView

@end
