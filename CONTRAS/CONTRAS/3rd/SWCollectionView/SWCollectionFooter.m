//
//  SWCollectionFooter.m
//  SRPLS
//
//  Created by Tan Le on 7/26/14.
//  Copyright (c) 2014 Tan Le. All rights reserved.
//

#import "SWCollectionFooter.h"

@implementation SWCollectionFooter

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
