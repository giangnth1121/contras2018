//
//  RoundRectView.m
//  Assign System
//
//  Created by Nguyen Ha Giang on 8/25/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "RoundRectView.h"

@interface RoundRectView ()
@property (nonatomic,strong) UIColor *bgColor;
@end

@implementation RoundRectView
{
    CAShapeLayer *shadowLayer;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    if ([backgroundColor isEqual:[UIColor clearColor]])
    {
        [super setBackgroundColor:backgroundColor];
        return;
    }
    else
    {
        self.bgColor = backgroundColor;
        [super setBackgroundColor:backgroundColor];
    }
}

-(void)setBgColor:(UIColor *)bgColor
{
    shadowLayer.fillColor = bgColor.CGColor;
    
    [shadowLayer setNeedsDisplay];
    _bgColor = bgColor;
}

-(void)setShadow:(BOOL)shadow
{
    _shadow = shadow;
    
    if (shadow)
    {
        CGMutablePathRef path = CGPathCreateMutable();
        CGMutablePathRef shadowPath = CGPathCreateMutable();
        
        CGPathAddRoundedRect(path, NULL, self.bounds, self.cornerRadius, self.cornerRadius);
        CGPathAddRoundedRect(shadowPath, NULL, self.bounds, self.cornerRadius, self.cornerRadius);
        
        if (!shadowLayer)
        {
            shadowLayer = [[CAShapeLayer alloc] init];
            shadowLayer.fillColor = self.bgColor.CGColor;
            shadowLayer.shadowColor = [UIColor grayColor].CGColor;
            shadowLayer.shadowRadius = 2;
            shadowLayer.shadowOpacity = 0.5;
            shadowLayer.shadowOffset = self.shadowOffset;
            shadowLayer.cornerRadius = self.cornerRadius;
            
            self.backgroundColor = [UIColor clearColor];
            
            if (self.borderColor)
            {
                shadowLayer.borderColor = self.borderColor.CGColor;
            }
            
            if (self.borderWidth != 0)
            {
                shadowLayer.borderWidth = self.borderWidth;
            }
            
            [self.layer insertSublayer:shadowLayer atIndex:0];
        }
        
        shadowLayer.path = path;
        shadowLayer.shadowPath = shadowPath;
        shadowLayer.frame = self.bounds;
        
        self.layer.cornerRadius = 0;
        self.layer.masksToBounds = NO;
        self.layer.borderWidth = 0;
    }
}

-(void)setShadowOffset:(CGSize)shadowOffset
{
    if (shadowLayer)
    {
        shadowLayer.shadowOffset = shadowOffset;
    }
    
    _shadowOffset = shadowOffset;
}

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    if (shadowLayer)
    {
        shadowLayer.cornerRadius = cornerRadius;
    }
    else
    {
        self.layer.cornerRadius = cornerRadius;
        self.layer.masksToBounds = YES;
    }
    
    _cornerRadius = cornerRadius;
}

-(void)setBorderColor:(UIColor *)borderColor
{
    if (shadowLayer)
    {
        shadowLayer.borderColor = borderColor.CGColor;
    }
    else
    {
        self.layer.borderColor = borderColor.CGColor;
    }
    
    _borderColor = borderColor;
}

-(void)setBorderWidth:(CGFloat)borderWidth
{
    if (shadowLayer)
    {
        shadowLayer.borderWidth = borderWidth;
    }
    else
    {
        self.layer.borderWidth = borderWidth;
    }
    
    _borderWidth = borderWidth;
}

@end
