//
//  LoginTableViewCell.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LoginTableViewCellDelegate<NSObject>

- (void)loginFacebook;
- (void)loginTwitter;
- (void)loginGmail;
@end

@interface LoginTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIButton *fbButton;
@property (nonatomic, weak) IBOutlet UIButton *twitterButton;
@property (nonatomic, weak) IBOutlet UIButton *googleButton;
@property (nonatomic, weak) id <LoginTableViewCellDelegate> delegate;
@end
