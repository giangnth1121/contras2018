//
//  NewCell.h
//  CONTRAS
//
//  Created by Giang Béo  on 2/28/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@property (nonatomic, weak) IBOutlet UILabel *lbDesc;
@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@end
