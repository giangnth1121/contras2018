//
//  ITTabbarView.h
//  SpecTee
//
//  Created by Nguyen Ha Giang on 3/14/17.
//  Copyright © 2017 ITLancerApp. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    TabHome = 0,
    TabJob = 1,
    TabProfile = 2,
    TabSetting = 3
}TabName;

@class ITTabbarView;
@protocol ITTabbarViewDelegate <NSObject>

@optional
- (void)tabBar:(ITTabbarView *)tabBar didSelectItemAtIndex:(NSUInteger)index;

@end
@interface ITTabbarView : UIView
@property (nonatomic, weak) IBOutlet UIView *view;
@property (nonatomic, assign) TabName selectedTabBarItemIndex;
@property (nonatomic, weak) id <ITTabbarViewDelegate> delegate;
- (void)setNumOfNewUpdate:(NSInteger)num ;
@end
