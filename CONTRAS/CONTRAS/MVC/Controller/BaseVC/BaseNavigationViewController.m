//
//  BaseNavigationViewController.m
//  TopShare
//
//  Created by ThanhVu on 7/19/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "BaseNavigationViewController.h"

#define debug 0
@interface BaseNavigationViewController ()

@end

@implementation BaseBarButtonItem
-(id) initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style tintColor:(UIColor*) tintColor target:(id)target action:(SEL)action{
    if (!(self = [self initWithTitle:title style:style target:target action:action])) {
        return nil;
    }
    [_button setTitleColor:tintColor forState:UIControlStateNormal];
    return self;
}
-(id) initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style target:(id)target action:(SEL)action{
    _button = [[UIButton alloc] init];
    
    [_button setTitle:[NSString stringWithFormat:@" %@ ", title] forState:UIControlStateNormal];
    
    [_button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    [_button sizeToFit];
 //   _button.frame = CGRectInset(_button.frame, -10, -10);
    _button.frame = (CGRect){0, 0, _button.frame.size};
    _parentView = [[UIView alloc] initWithFrame:_button.bounds];
    
    [_parentView addSubview:_button];
    
    return [super initWithCustomView:_parentView];
}

-(id) initWithImageNamed:(NSString *)imageNamed target:(id)target action:(SEL)action{
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    [_button setImage:[UIImage imageNamed:imageNamed] forState:UIControlStateNormal];
    //    [_button setTitle:@"" forState:UIControlStateNormal];
    [_button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    [_button sizeToFit];
//    _button.frame = CGRectInset(_button.frame, -10, -10);
    _button.frame = (CGRect){0, 0, _button.frame.size};
    _parentView = [[UIView alloc] initWithFrame:_button.bounds];
    [_parentView addSubview:_button];
    
    return [super initWithCustomView:_parentView];
}

-(void)setTitleTextAttributes:(NSDictionary<NSString *,id> *)attributes forState:(UIControlState)state{
    [super setTitleTextAttributes:attributes forState:state];
    [_button setAttributedTitle:[[NSAttributedString alloc] initWithString:_button.titleLabel.text attributes:attributes] forState:state];
    
}
-(void)setTintColor:(UIColor *)tintColor{
    [super setTintColor:tintColor];
    [_button setTintColor:self.tintColor];
}
#pragma mark - property setters -

-(void) setTitlePositionAdjustment:(UIOffset)adjustment forBarMetrics:(UIBarMetrics)barMetrics{
    //    [_button sizeToFit];
    //    _parentView.bounds = CGRectMake(0.0, 0.0, _button.bounds.size.width - (adjustment.horizontal * 2.0), _button.bounds.size.height - (adjustment.vertical * 2.0));
    
    _button.frame = CGRectOffset(_button.frame, adjustment.horizontal, adjustment.vertical);
}

-(void)setButtonVerticalPositionAdjustment:(UIOffset)adjustment forBarMetrics:(UIBarMetrics)barMetrics{
    //    _parentView.bounds = CGRectMake(0.0, 0.0, _button.bounds.size.width , _button.bounds.size.height);
    _button.frame = CGRectOffset(_button.frame, adjustment.horizontal, adjustment.vertical);
}
@end

@implementation BaseNavigationViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self config];
}
-(BOOL)prefreStatusBarHidden
{
    return YES;
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return [self.topViewController preferredStatusBarStyle];
}

#pragma mark - CONFIG
-(void) config
{
}

#pragma mark - LoadData
-(void) loadData
{
}

#pragma mark - Action & Behavior

@end

@implementation UINavigationController (AN)

- (void)setLeftNavigationButtonWithTitle:(NSString *)title action:(SEL)selector {
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self.topViewController action:selector];
        [barButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
        [barButton setTitlePositionAdjustment:UIOffsetMake(-10, 0) forBarMetrics:UIBarMetricsDefault];
        self.topViewController.navigationItem.leftBarButtonItem = barButton;
    }
    
}

- (void)setLeftNavigationButtonWithTitle:(NSString *)title tintColor:(UIColor*)tintColor action:(SEL)selector {
    
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone tintColor:tintColor target:self.topViewController action:selector];
        [barButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
        [barButton setTitlePositionAdjustment:UIOffsetMake(-10, 0) forBarMetrics:UIBarMetricsDefault];
        self.topViewController.navigationItem.leftBarButtonItem = barButton;
    }
}

- (void)setLeftNavigationButtonImage:(NSString*)imageName action:(SEL)selector {
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithImageNamed:imageName target:self.topViewController action:selector];
        [barButton setButtonVerticalPositionAdjustment:UIOffsetMake(-5,0) forBarMetrics:UIBarMetricsDefault];
        [self.topViewController.navigationItem setLeftBarButtonItem:barButton];
    }
    
}

- (void)setRightNavigationButtonWithTitle:(NSString *)title tintColor:(UIColor*)tintColor action:(SEL)selector {
    
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone tintColor:tintColor target:self.topViewController action:selector];
        [barButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
        [barButton setTitlePositionAdjustment:UIOffsetMake(5, 0) forBarMetrics:UIBarMetricsDefault];
        self.topViewController.navigationItem.rightBarButtonItem = barButton;
    }
}

- (void)setRightNavigationButtonWithTitle:(NSString *)title tintColor:(UIColor*)tintColor font:(UIFont*) font action:(SEL)selector {
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone tintColor:tintColor target:self.topViewController action:selector];
        [barButton setTitleTextAttributes:@{NSFontAttributeName:font} forState:UIControlStateNormal];
        [barButton setTitlePositionAdjustment:UIOffsetMake(5, 0) forBarMetrics:UIBarMetricsDefault];
        self.topViewController.navigationItem.rightBarButtonItem = barButton;
    }
}


- (void)setRightNavigationButtonWithTitle:(NSString *)title action:(SEL)selector {
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStyleDone target:self.topViewController action:selector];
        [barButton setTitleTextAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15]} forState:UIControlStateNormal];
        [barButton setTitlePositionAdjustment:UIOffsetMake(5, 0) forBarMetrics:UIBarMetricsDefault];
        self.topViewController.navigationItem.rightBarButtonItem = barButton;
    }
    
}

- (void)setRightNavigationButtonImage:(NSString*)imageName action:(SEL)selector {
    if (self.topViewController) {
        BaseBarButtonItem* barButton = [[BaseBarButtonItem alloc] initWithImageNamed:imageName target:self.topViewController action:selector];
        [self.topViewController.navigationItem setRightBarButtonItem:barButton];
    }
    
}

@end
