//
//  ListNotifyVC.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListNotifyVC : BaseViewController
@property (nonatomic, strong) IBOutlet UITableView *notifyTableView;
@end
