//
//  LoginSelectViewController.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/17/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "LoginSelectViewController.h"
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "AppDelegate.h"
@import TwitterKit;
@interface LoginSelectViewController () <GIDSignInUIDelegate,GIDSignInDelegate>

@end

@implementation LoginSelectViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self config];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches allObjects].firstObject;
    CGPoint touchInView = [touch locationInView:self.view];
    CGPoint touchInContainerView = [self.view convertPoint:touchInView toView:self.contentContainerView];
    
    if (CGRectContainsPoint(self.contentContainerView.bounds, touchInContainerView) == NO)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - Config

-(void) config
{
    
}

#pragma mark - LoadData

#pragma mark - IBAction
- (IBAction)termButtonDidTap:(id)sender {
}

- (IBAction)privacyPolicyButtonDidTap:(id)sender {
}

- (IBAction)loginFacebookButtonDidTap:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self loginFacebook];
                             }];
}

- (IBAction)loginTwitterButtonDidTap:(id)sender {
 
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self loginTwitter];
                             }];
   
}

- (IBAction)loginGoogleButtonDidTap:(id)sender {
    [self dismissViewControllerAnimated:YES
                             completion:^{
                                 [self loginGmail];
                             }];
}

- (IBAction)signInWithEmailButtonDidTap:(id)sender {
}

- (IBAction)subcribeButtonDidTap:(UIButton *)sender
{
    sender.selected = !sender.selected;
}

#pragma mark - LoginSelected Delegate
- (void)reloadWhenLoginSuccess {
   // [self.videosTableView reloadData];
}

- (void)loginFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    // Process error
                                    NSLog(@"error %@",error);
                                } else if (result.isCancelled) {
                                    // Handle cancellations
                                    NSLog(@"Cancelled");
                                } else {
                                    if ([result.grantedPermissions containsObject:@"email"]) {
                                        // Do work
                                        NSLog(@"%@",result);
                                        [self fetchUserInfoFromFB];
                                    }
                                }
                            }];
}
-(void)fetchUserInfoFromFB
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        NSString *social_token = [[FBSDKAccessToken currentAccessToken]tokenString];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                           parameters:@{@"fields": @"id,name,email,picture.type(large),location,birthday"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
                 
                 NSDictionary *dictUser = (NSDictionary *)result;
                 UserModel *userModel  =  [[UserModel alloc] init];// = [[UserModel alloc] initWithDictionary:result error:nil];
                 userModel.email = dictUser[@"email"];
                 userModel.username = dictUser [@"name"];
                 userModel.avatar = [NSString stringWithFormat:@"%@",result[@"picture"][@"data"][@"url"]];
                 
                 [self socialLoginWithUser:userModel
                                 loginType:kLoginFacebook
                              social_token:social_token];
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
    }
}

- (void)loginTwitter {
    // Objective-C
    // Objective-C
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
            NSString *username           = [NSString stringWithFormat:@"%@",[session userName]];
            //  NSString *userID             = [NSString stringWithFormat:@"%@",[session userID]];
            UserModel *userModel = [[UserModel alloc] init];
            userModel.username = username;
            userModel.userID = [session userID];
            
            [self socialLoginWithUser:userModel
                            loginType:kLoginTwitter
                         social_token:@""];
            
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}
- (void)loginGmail {
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate   = self;
    [[GIDSignIn sharedInstance] signIn];
    //  [GIDSignIn sharedInstance].shouldFetchGoogleUserEmail = YES;
    [GIDSignIn sharedInstance].scopes = @[ @"profile" ];
}
- (void)socialLoginWithUser:(UserModel *)user
                  loginType:(loginType)loginType
               social_token:(NSString *)social_token{
//    [GiFHUD setGifWithImageName:@"ic_loading.gif"];
//    [GiFHUD show];
//    [APIController signInWithUser:user
//                        loginType:loginType
//                  completeHandler:^(UserModel *userLogin) {
//                      NSLog(@"%@",userLogin);
//                      user.userID = userLogin.userID;
//                      [Utils saveDataUser:user
//                                loginType:loginType];
//                      [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginSuccess object:nil];
//                      [self reloadWhenLoginSuccess];
//                      [GiFHUD dismiss];
//                  } failure:^(APIResult *apiResult) {
//
//                      [GiFHUD dismiss];
//                      [self handleAPIError:apiResult];
//                  }];
}
#pragma mark - GIDSignInDelegate
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *email = user.profile.email;
    
    NSString *fullName = user.profile.name;
    //    NSString *givenName = user.profile.givenName;
    //    NSString *familyName = user.profile.familyName;
    NSLog(@"LOGIN GOOGLE:%@",user);
    
    NSURL *url = (user.profile.hasImage) ?  [user.profile imageURLWithDimension:100] : [NSURL URLWithString:@""];
    NSString *avatar = url.absoluteString;
    NSLog(@"url : %@",url);
    UserModel *userModel = [[UserModel alloc] init];
    userModel.email         = email;
    userModel.userID            = [userId integerValue];
    userModel.username          = fullName;
    userModel.avatar        = avatar;
    
    [self socialLoginWithUser:userModel
                    loginType:kLoginGoogle
                 social_token:idToken];
    // ...
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}
@end
