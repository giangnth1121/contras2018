//
//  Macro.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/13/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#ifndef Macro_h
#define Macro_h

#import "macros_all.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define BASE_URL_API @"xxx"


#define StringIsNullOrEmpty(x)    ((x == nil || [x isEqual:[NSNull null]] || [x length] == 0) ? YES : NO)
typedef enum {
    kLoginGoogle = 1,
    kLoginFacebook = 2,
    kLoginTwitter = 3,
    kSignInEmail = 4,
    kLogout
} loginType;
typedef enum {
    cityType = 1,
    districtType = 2,
    streetType = 3
} listType;

typedef enum {
    allType = 1,
    videoType = 2,
    userType = 3
} searchType;

#define IS_LOGINED  @"isLogined"
#define KEY_USER_ID  @"currentUserID"
#define KEY_LOGINTYPE @"loginType"
#define KEY_SETTINGVIDEOTYPE @"settingAutoPlay"
#define KEY_USERNAME  @"userName"
#define KEY_PASSWORD  @"Password"
#define KEY_LINKAVATAR  @"AvatarUser"
#define KEY_ACCESS_TOKEN                    @"AccessToken"
#define KEY_AUTHORIZATIONDATA               @"Authorization-data"
#define HEADER_ACCESS_TOKEN                 @"access_token"
#define DEFAULT_AVATAR                      @"default_avatar"
#define KEY_LANGUAGUECODE                   @"AppleLanguages"

#pragma mark - SCREEN INFORMATION

/** Float: Portrait Screen Height **/
#define SCREEN_HEIGHT_PORTRAIT ([[UIScreen mainScreen] bounds].size.height)

/** Float: Portrait Screen Width **/
#define SCREEN_WIDTH_PORTRAIT ([[UIScreen mainScreen] bounds].size.width)

/** Float: Landscape Screen Height **/
#define SCREEN_HEIGHT_LANDSCAPE ([[UIScreen mainScreen] bounds].size.width)

/** Float: Landscape Screen Width **/
#define SCREEN_WIDTH_LANDSCAPE ([[UIScreen mainScreen] bounds].size.height)

/** CGRect: Portrait Screen Frame **/
#define SCREEN_FRAME_PORTRAIT (CGRectMake(0, 0, SCREEN_WIDTH_PORTRAIT, SCREEN_HEIGHT_PORTRAIT))

/** CGRect: Landscape Screen Frame **/
#define SCREEN_FRAME_LANDSCAPE (CGRectMake(0, 0, SCREEN_WIDTH_LANDSCAPE, SCREEN_HEIGHT_LANDSCAPE))

/** Float: Screen Scale **/
#define SCREEN_SCALE ([[UIScreen mainScreen] scale])

/** CGSize: Screen Size Portrait **/
#define SCREEN_SIZE_PORTRAIT (CGSizeMake(SCREEN_WIDTH_PORTRAIT * SCREEN_SCALE, SCREEN_HEIGHT_PORTRAIT * SCREEN_SCALE))

/** CGSize: Screen Size Landscape **/
#define SCREEN_SIZE_LANDSCAPE (CGSizeMake(SCREEN_WIDTH_LANDSCAPE * SCREEN_SCALE, SCREEN_HEIGHT_LANDSCAPE * SCREEN_SCALE))

#pragma mark - COLOR

#define MAIN_COLOR UIColorFromRGB(0x0cb8b6)
#define MAIN_BGCOLOR UIColorFromRGB(0xF9F9F9)

/** UIColor: Color From Hex **/
#define COLOR_FROM_HEX(rgbValue)([UIColor UIColorFromRGB:rgbValue])

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
/** UIColor: Color from RGB **/

#define RGB(r, g, b)[UIColor colorWithRed : r / 255.0 green : g / 255.0 blue : b / 255.0 alpha : 1]

/** UIColor: Color from RGBA **/

#define COLOR_FROM_RGBA(r, g, b, a) ([UIColor colorWithRed:r / 255.0 green:g / 255.0 blue:b / 255.0 alpha:a])

#define RGBA(r, g, b, a)[UIColor colorWithRed : r / 255.0 green : g / 255.0 blue : b / 255.0 alpha : a]

#pragma mark - DEGREES, RADIANS, MEASUREMENT UNITS CONVERTERS, SYSTEM VERSIONS

/** Degrees to Radian **/
#define DEGREES_TO_RADIANS(degrees) ((degrees) / 180.0 * M_PI)

/** Radians to Degrees **/
#define RADIANS_TO_DEGREES(radians) ((radians) * (180.0 / M_PI))

/** Miles to Kilometers **/
#define MILES_TO_KILOMETERS (miles)(miles * 1.60934)

/** Miles to Feets **/
#define MILES_TO_FEETS (miles)(miles * 5280)

/** Get image by name*/
#define Image(x)  [UIImage imageNamed:x]

/** String by format*/
#define $S(format, ...) [NSString stringWithFormat:format, ## __VA_ARGS__]

/** String is null or empty**/
#define StringIsNullOrEmpty(x)    ((x == nil || [x isEqual:[NSNull null]] || [x length] == 0) ? YES : NO)


#define NIL_IF_NULL(foo) ((foo == [NSNull null]) ? nil : foo)

#define NULL_IF_NIL(foo) ((foo == nil) ? [NSNull null] : foo)

#define EMPTY_IF_NIL(foo) ((foo == nil) ? @"" : foo)

#define EMPTY_IF_NULL(foo) ((foo == [NSNull null]) ? @"" : foo)

#define EMPTY_IF_NULL_OR_NIL(foo) (([foo isEqual:[NSNull null]]  || foo == nil) ? @"" : foo)

#define SYSTEM_VERSION_EQUAL_TO(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)

#define SYSTEM_VERSION_GREATER_THAN(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)

#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define IMAGENAMED(NAME)       [UIImage imageNamed:NAME]


#endif /* Macro_h */
