//
//  NewVC.h
//  CONTRAS
//
//  Created by Giang Béo  on 2/28/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "BaseViewController.h"

@interface NewVC : BaseViewController <UITableViewDataSource, UITableViewDelegate>
{
   __weak IBOutlet UITableView *_newsTableView;
}
@end
