//
//  AppDelegate.h
//  SACH
//
//  Created by Giang Béo  on 12/24/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ITTabbarController.h"
#import "Macro.h"
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong,nonatomic) BaseNavigationViewController *nav;
@property (strong,nonatomic) ITTabbarController *tabbarController;

@property (assign,nonatomic) loginType loginType;
@property (assign, nonatomic) BOOL isErrorNetwork;
+ (AppDelegate *)shareAppDelegate;

- (void)hideTabbar:(BOOL)isHide;
@end

