//
//  NotifyCell.h
//  TopShare
//
//  Created by Giang Béo  on 11/30/17.
//  Copyright © 2017 Retech. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotifyCell : UITableViewCell
@property (nonatomic, strong) IBOutlet UIImageView *notifyImg;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@end
