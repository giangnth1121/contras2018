//
//  Global.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "Global.h"
#import "AppDelegate.h"
static Global *_instance;
@implementation Global

+(Global*)share
{
    if (_instance) {
        return _instance;
    }
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[Global alloc] init];
    });
    return _instance;
}

#pragma mark - UIAlertAction
- (void) showActionSheetWithTitle:(NSString *)title inView:(id) mySelf title1:(NSString *)title1  action1:(void (^) (void))handleAction1 title2:(NSString *)title2 action2:(void (^) (void))handleAction2{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *btn1 = [UIAlertAction actionWithTitle:title1 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handleAction1) {
            handleAction1();
        }
    }];
    UIAlertAction *btn2 = [UIAlertAction actionWithTitle:title2 style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        if (handleAction2) {
            handleAction2();
        }
    }];
    
    [alertController addAction:btn1];
    [alertController addAction:btn2];
    [alertController addAction:cancel];
    
    [mySelf presentViewController:alertController animated:YES completion:nil];
}
- (void) showMessage:(NSString *)message inView:(id)mySelf{
    
    if (StringIsNullOrEmpty(message)) {
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *OK = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    
    [alertController addAction:OK];
    
    [mySelf presentViewController:alertController animated:YES completion:nil];
}
- (void)showAlertDisconnectSocialWithMessage:(NSString *) message  action:(void (^) (void))handleAction  inView:(id)mySelf{
    
    if (StringIsNullOrEmpty(message)) {
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"Are you sure want to disconnect with %@ ?",message] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *OK = [UIAlertAction actionWithTitle:@"Disconnect" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
        if (handleAction) handleAction();
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:OK];
    
    [mySelf presentViewController:alertController animated:YES completion:nil];
}
- (void)showAlertWithMessage:(NSString *) message  titleButton :(NSString *)titleButton action:(void (^) (void))handleAction  inView:(id)mySelf{
    
    if (StringIsNullOrEmpty(message)) {
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:nil message:message preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *OK = [UIAlertAction actionWithTitle:titleButton style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
        if (handleAction) handleAction();
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:OK];
    
    [mySelf presentViewController:alertController animated:YES completion:nil];
}
- (void)showAlertWithTitle:(NSString *) title message:(NSString *)message  titleButton :(NSString *)titleButton action:(void (^) (void))handleAction  inView:(id)mySelf{
    
    if (StringIsNullOrEmpty(message)) {
        return;
    }
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];

    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
    }];
    
    UIAlertAction *OK = [UIAlertAction actionWithTitle:titleButton style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [alertController dismissViewControllerAnimated:YES completion:nil];
        if (handleAction) handleAction();
    }];
    
    [alertController addAction:cancel];
    [alertController addAction:OK];
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.alignment = NSTextAlignmentLeft;
    
    NSMutableAttributedString *atrStr = [[NSMutableAttributedString alloc] initWithString:message attributes:@{NSParagraphStyleAttributeName:paraStyle,NSFontAttributeName:[UIFont systemFontOfSize:14.0]}];
    
    [alertController setValue:atrStr forKey:@"attributedMessage"];
    [mySelf presentViewController:alertController animated:YES completion:nil];
}
- (BOOL)isHasAccountFB {
    return [AppDelegate shareAppDelegate].loginType == kLoginFacebook;
}
- (BOOL)isHasAccountTwitter {
    return [AppDelegate shareAppDelegate].loginType == kLoginTwitter;
}
- (BOOL)isHasAccountGoogle {
    return [AppDelegate shareAppDelegate].loginType == kLoginGoogle;
}

@end
