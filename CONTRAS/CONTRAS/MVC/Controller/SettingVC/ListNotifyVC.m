//
//  ListNotifyVC.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "ListNotifyVC.h"
#import "NotifyCell.h"

@interface ListNotifyVC () <UITableViewDelegate, UITableViewDataSource>

@end

@implementation ListNotifyVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:YES];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}
- (void)configUI {
    
    self.title = @"Notifications";
    [self setBackNavigationButton];
    self.notifyTableView.dataSource = self;
    self.notifyTableView.delegate = self;
    self.notifyTableView.rowHeight = UITableViewAutomaticDimension;
    self.notifyTableView.estimatedRowHeight = 290;
    self.notifyTableView.tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.1, 0.1)];
    self.notifyTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.1, 0.1)];
    [self.notifyTableView registerNib:[UINib nibWithNibName:@"NotifyCell" bundle:nil] forCellReuseIdentifier:@"NotifyCell"];
    self.notifyTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  10;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NotifyCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotifyCell"];
    return cell;
}
@end
