//
//  NSDictionary+Safe.m
//  TGS
//
//  Created by Nguyen Ha Giang on 2/17/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "NSDictionary+Safe.h"

@implementation NSDictionary (Safe)

-(id)objForKey:(id)key
{
    id value = [self objectForKey:key];
    
    if ([value isKindOfClass:[NSNull class]])
    {
        return nil;
    }
    
    return value;
}

@end

@implementation NSDictionary (Debug)

-(NSString *)toJsonString
{
    return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:self options:NSJSONWritingPrettyPrinted error:nil] encoding:NSUTF8StringEncoding];
}

@end
