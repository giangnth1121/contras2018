//
//  OrderVC.m
//  SACH
//
//  Created by Giang Béo  on 12/28/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "OrderVC.h"
#import "ListConfirmOrderVC.h"
#import "OrderCell.h"


@interface OrderVC () <UITableViewDataSource, UITableViewDelegate,OrderCellDelegate>
{
    NSArray *sortedKeys;
    NSString *key;
    NSArray *typeSourceArrays;
    NSDictionary *sourceDict;
    NSMutableArray *ordersArray;
    NSInteger _totalPrice;
}
@end

@implementation OrderVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:YES];
    [_orderTableView reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self initData];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [ordersArray removeAllObjects];
    
    [_orderTableView reloadData];
}
- (void)configUI {
    self.title = @"ĐẶT HÀNG";
    [self setBackNavigationButton];
    self.orderTableView.dataSource = self;
    self.orderTableView.delegate = self;
    [self.orderTableView registerNib:[UINib nibWithNibName:@"OrderCell" bundle:nil] forCellReuseIdentifier:@"OrderCell"];
    self.orderTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.lbTotal.font = [UIFont boldSystemFontOfSize:17];
    self.lbTotal.textColor = [UIColor purpleColor];
    self.lbTotal.text = @"0đ";
    self.btnConfirm.layer.cornerRadius = 5.0f;
    self.btnConfirm.layer.masksToBounds = YES;
    self.btnConfirm.backgroundColor = [UIColor purpleColor];
    self.btnConfirm.alpha = 0.5f;
    self.btnConfirm.userInteractionEnabled = NO;
}
- (void)initData {
    ordersArray = [[NSMutableArray alloc] init];
    // read file Setting.plist
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"Items" ofType:@"plist"];
    sourceDict = [NSDictionary dictionaryWithContentsOfFile:plistPath];
    
}
- (void)setSectionData:(NSInteger)section
{
    sortedKeys = [[sourceDict allKeys] sortedArrayUsingSelector:@selector(localizedStandardCompare:)];
    key = [sortedKeys objectAtIndex:section];
    typeSourceArrays = [sourceDict objectForKey:key];
    
}
- (IBAction)confirmOrderTapped:(id)sender {
    
    if ([ordersArray count] == 0) {
        [[Global share] showMessage:@"Vui lòng chọn đồ" inView:self];
        return;
    }
    ListConfirmOrderVC *orderVC = [[ListConfirmOrderVC alloc] init];
    orderVC.ordersArray = ordersArray;
    [self.navigationController pushViewController:orderVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  [[sourceDict allKeys] count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // set Data for section
    [self setSectionData:section];
    
    return [typeSourceArrays count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderCell *orderCell = [tableView dequeueReusableCellWithIdentifier:@"OrderCell" forIndexPath:indexPath];
    [self setSectionData:indexPath.section];
    orderCell.delegate = self;
    NSDictionary *dict = [typeSourceArrays objectAtIndex:indexPath.row];
    orderCell.idxRow  = indexPath.row;
    [orderCell setContentCell:dict];
    return orderCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
#pragma mark - OrderCellDelegate
- (void)updateFruitInfo:(NSDictionary*)info idx:(NSInteger)idx{

    [ordersArray addObject:info];
    for (int i =0 ;i < [ordersArray count]; i++) {
        NSDictionary *dict = ordersArray[i];
        if ([info[@"id"] integerValue] == [dict[@"id"] integerValue]) {
            
            [ordersArray removeObject:dict];
            [ordersArray addObject:info];
        }
    }
    [self getTotalPrice];
}
- (void)getTotalPrice {
    _totalPrice = 0;
    for (int i = 0; i < [ordersArray count]; i ++) {
        NSDictionary *dict = ordersArray[i];
        _totalPrice += [dict[@"price"] integerValue]* [dict[@"number"] integerValue] ;
    }
    self.lbTotal.text = [NSString stringWithFormat:@"%@",[Utils formatCurrency:_totalPrice] ];
    [self checkStatusOrder];
}

- (void)checkStatusOrder {
    self.btnConfirm.userInteractionEnabled   = _totalPrice > 0;
    self.btnConfirm.alpha                    = (_totalPrice > 0) ? 1.0f : 0.5f;
}
@end
