//
//  StringDefine.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/13/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#ifndef StringDefine_h
#define StringDefine_h

#define		STATUS_LOADING						1
#define		STATUS_FAILED						2
#define		STATUS_SUCCEED						3

static NSString * const kRootUrl                        = @"http://topshare.onclip.vn/news/";

static NSString * const kPOST                           = @"POST";
static NSString * const kGET                            = @"GET";

static NSString * const kGetListVideo                   = @"loadvideo";
static NSString * const kGetListCate                    = @"getListCate";
static NSString * const kGetListArticleByCateId         = @"listArticleByCateId";
static NSString * const kGetArticleDetail               = @"getArticleDetail";
static NSString * const kPostComment                    = @"postComment";
static NSString * const kPostReComment                  = @"postReComment";
static NSString * const kRegisterUser                   = @"registerUser";
static NSString * const kSavefarvorite                  = @"savefarvorite";
static NSString * const kListAriclefarvorite            = @"listAriclefarvorite";
static NSString * const kAddgomarket                    = @"addgomarket";
static NSString * const kListgomarket                   = @"listgomarket";
static NSString * const kListSearchArticle              = @"listSearchArticle";
static NSString * const kListKeyWord                    = @"listKeyWord";
static NSString * const kListNotification               = @"listNotification";
static NSString * const kRegisterDevice                 = @"registerDevice";
static NSString * const kProfile                        = @"profile";
static NSString * const kListStore                      = @"listStore";
static NSString * const kGetChartsUser                  = @"get-charts-user";

static NSString * const kCateId                         = @"cateId";
static NSString * const kPage                           = @"page";
static NSString * const kArticleId                      = @"articleId";
static NSString * const kContent                        = @"content";
static NSString * const kUserId                         = @"userId";
static NSString * const kCommentId                      = @"commentId";
static NSString * const kFullName                       = @"fullName";
static NSString * const kLinkImage                      = @"linkImage";
static NSString * const kKeyword                        = @"keyword";
static NSString * const kDeviceID                       = @"DeviceID";
static NSString * const kToken                          = @"Token";
static NSString * const kOS                             = @"OS";

static NSString * const kIsLogined                             = @"IsLogined";

static NSString *kNotificationLoginSuccess   = @"LoginSuccessNotification";
static NSString *kNotificationLogout         = @"LogoutSuccessNotification";
#endif /* StringDefine_h */
