//
//  OrderCell.m
//  SACH
//
//  Created by Giang Béo  on 12/28/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "OrderCell.h"

@implementation OrderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initUI];
    // Initialization code
}
- (void)initUI {
    self.iconImg.layer.cornerRadius = 5.0f;
    self.iconImg.layer.masksToBounds = YES;
    self.reduceButton.layer.borderWidth = 1.0f;
    self.reduceButton.layer.borderColor = [[UIColor purpleColor] CGColor];
    self.increaseButton.layer.borderWidth = 1.0f;
    self.increaseButton.layer.borderColor = [[UIColor purpleColor] CGColor];
    self.numberOfOrderButton.layer.borderWidth = 1.0f;
    self.numberOfOrderButton.layer.borderColor = [[UIColor purpleColor] CGColor];
    self.priceLabel.textColor = [UIColor purpleColor];
    self.totalPriceLabel.textColor = [UIColor purpleColor];
    self.totalPriceLabel.font = [UIFont boldSystemFontOfSize:15];
    
}
- (void)setContentCell:(NSDictionary *)dict {
    self.infoFruit = dict;
    self.price =  [dict[@"price"] integerValue];
    [self.iconImg setImage:[UIImage imageNamed:dict[@"image"]]];
    self.titleLabel.text = dict[@"title"];
    self.priceLabel.text = [NSString stringWithFormat:@"%@", [Utils formatCurrency:[dict[@"price"] integerValue]]];
    [self.numberOfOrderButton setTitle:@"0" forState:UIControlStateNormal];
    [self setTotalPriceWithNumber:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setTotalPriceWithNumber:(NSInteger)number {
    
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%@",[Utils formatCurrency:self.price*number]];
}
- (IBAction)increaseTapped:(UIButton *)sender {
    NSInteger number = [self.numberOfOrderButton.titleLabel.text integerValue];
    [self.numberOfOrderButton setTitle:[NSString stringWithFormat:@"%ld",number+1] forState:UIControlStateNormal];
    [self setTotalPriceWithNumber:number+1];
    if (self.delegate) {
        NSMutableDictionary *dictInfo = [self.infoFruit mutableCopy];
        NSInteger price = [dictInfo[@"price"] integerValue];
        [dictInfo setValue:[NSString stringWithFormat:@"%ld",number +1] forKey:@"number"];
        [dictInfo setValue:[NSString stringWithFormat:@"%ld",(number+1) *price] forKey:@"total"];
        
        [self.delegate updateFruitInfo:dictInfo idx:self.idxRow];
        //  [self.delegate numberOrderWithIndex:self.idxRow number:number+1];
    }

}
- (IBAction)reduceTapped:(UIButton *)sender {
    
    NSInteger number = [self.numberOfOrderButton.titleLabel.text integerValue];
    
    [self.numberOfOrderButton setTitle:number < 1 ? @"0" :[NSString stringWithFormat:@"%ld",number-1]  forState:UIControlStateNormal];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%ld",self.price*(number -1)];
    
    [self setTotalPriceWithNumber:number < 1 ? 0 :number-1];
    if (self.delegate) {
        NSMutableDictionary *dictInfo = [self.infoFruit mutableCopy];
        NSInteger price = [dictInfo[@"price"] integerValue];
        [dictInfo setValue:[NSString stringWithFormat:@"%ld",number < 1 ? 0 :number-1] forKey:@"number"];
        [dictInfo setValue:[NSString stringWithFormat:@"%ld",(number < 1 ? 0 :number-1) *price] forKey:@"total"];
        
        [self.delegate updateFruitInfo:dictInfo idx:self.idxRow];
    }
}
@end
