//
//  OrderCell.h
//  SACH
//
//  Created by Giang Béo  on 12/28/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OrderCellDelegate <NSObject>
@optional

- (void)updateFruitInfo:(NSDictionary*)info idx:(NSInteger)idx;
@end

@interface OrderCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *iconImg;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *localizePriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *priceLabel;
@property (nonatomic, weak) IBOutlet UILabel *totalPriceLabel;
@property (nonatomic, weak) IBOutlet UILabel *valueLabel;
@property (nonatomic, weak) IBOutlet UIButton *increaseButton;
@property (nonatomic, weak) IBOutlet UIButton *reduceButton;
@property (nonatomic, weak) IBOutlet UIButton *numberOfOrderButton;

@property (nonatomic, weak) id <OrderCellDelegate> delegate;
@property (nonatomic) NSInteger idxRow;
@property (nonatomic) NSDictionary *infoFruit;
@property (nonatomic) NSInteger price;
- (void)setContentCell:(NSDictionary *)dict;
@end
