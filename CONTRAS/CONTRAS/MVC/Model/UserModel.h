//
//  UserModel.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

@interface UserModel : BaseModel

@property (nonatomic) NSInteger userID;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* token;
@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* fullName;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString *fbID;
@property (nonatomic, strong) NSString *twID;

@property (nonatomic, strong) NSString* avatar;
@property (nonatomic) loginType loginType;
@end
