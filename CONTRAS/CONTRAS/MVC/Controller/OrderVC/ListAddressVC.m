//
//  ListAddressVC.m
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "ListAddressVC.h"
#import "AddressCell.h"
#import "AddNewAddressVC.h"
#import "TranferOrderVC.h"

@interface ListAddressVC () <UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *_addressArray;
}
@property (weak, nonatomic) IBOutlet UITableView *addressTableView;

@end

@implementation ListAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    
    self.title = @"ĐỊA CHỈ NHẬN HÀNG";
    [self setBackNavigationButton];
    self.addressTableView.dataSource = self;
    self.addressTableView.delegate = self;
    [self.addressTableView registerNib:[UINib nibWithNibName:@"AddressCell" bundle:nil] forCellReuseIdentifier:@"AddressCell"];
    
    _addressArray = [[NSMutableArray alloc] init];
    [self setRightButtonBarWithImage:@"ic_plus" action:@selector(addNewAddress)];
    self.btnNext.layer.cornerRadius = 5.0f;
    self.btnNext.layer.masksToBounds = YES;
    self.btnNext.backgroundColor = [UIColor purpleColor];
}

- (void)addNewAddress {
    
    AddNewAddressVC *addNewAddressVC = [[AddNewAddressVC alloc] init];
    [self.navigationController pushViewController:addNewAddressVC animated:YES];
}

- (IBAction)goToTranferOrderTapped:(id)sender {
    
    TranferOrderVC *orrderVC = [[TranferOrderVC alloc] init];
    orrderVC.ordersArray = self.ordersArray;
    [self.navigationController pushViewController:orrderVC animated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 1;// [_addressArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AddressCell *addressCell = [tableView dequeueReusableCellWithIdentifier:@"AddressCell" forIndexPath:indexPath];
    return addressCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 120;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        [_addressArray removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
}

@end
