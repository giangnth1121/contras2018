//
//  NSString+Util.h
//  TGS
//
//  Created by Nguyen Ha Giang on 2/20/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Util)
-(NSURL *) toURL;
-(NSAttributedString *) strikeAttributed;

-(NSArray<NSTextCheckingResult *> *) matchWithPattern:(NSString *)pattern;

-(BOOL) checkWithPattern:(NSString *)pattern;
@end
