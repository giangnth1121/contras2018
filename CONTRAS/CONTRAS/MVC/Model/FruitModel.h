//
//  FruitModel.h
//  SACH
//
//  Created by Giang Béo  on 12/29/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "BaseModel.h"

@interface FruitModel : BaseModel
@property (nonatomic) NSInteger fruitId;
@property (nonatomic) NSInteger price;
@property (nonatomic) NSInteger number;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* image;
@property (nonatomic, strong) NSString* value;

@end
