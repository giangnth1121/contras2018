//
//  GiftImageCell.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/29/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "GifImageCell.h"

@implementation GifImageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.indicatorView setHidden:YES];
    self.avatarImg.layer.cornerRadius = self.avatarImg.frame.size.width/2;
    self.avatarImg.layer.masksToBounds = YES;
    self.videoPlaceHolderImageView.clipsToBounds = YES;
    self.GIFImgView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setContentCellBeforeAnimate:(VideoModel *)video {
    self.GIFLabel.text = @"GIF";
    self.video = video;
    self.nameLabel.text = video.title;
    NSString *imageGIF = self.video.img;
    NSString* encodedUrl = [imageGIF stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    [self.videoPlaceHolderImageView sd_setImageWithURL:[NSURL URLWithString:encodedUrl] placeholderImage:[UIImage imageNamed:@"ic_placeholder"]];
    [self.avatarImg sd_setImageWithURL:[NSURL URLWithString:video.avarta] placeholderImage:[UIImage imageNamed:@"ic_profile"]];
    self.userNameLabel.text = video.author;
    [self bindingVideoInfo];
    self.GIFImgView.animatedImage = nil;
}

- (void)setContentCell:(VideoModel *)video {
//    self.video = video;
//    self.GIFLabel.text = @"";
//    NSURL *url  = [NSURL URLWithString:video.clip];
//   // FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:url]];
//    [self loadAnimatedImageWithURL:url completion:^(FLAnimatedImage *animatedImage) {
//        self.GIFImgView.animatedImage  = animatedImage;
//    }];
    
}
-(void)loadGIFImageTapped:(id)sender
{
    [self.indicatorView setHidden:NO];
    [self.indicatorView startAnimating];
    self.GIFLabel.text = @"";
    NSString *clip = self.video.clip;
    NSString* encodedUrl = [clip stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURL *url  = [NSURL URLWithString:encodedUrl];
    NSLog(@"url GIF %@",url);
    // FLAnimatedImage *image = [FLAnimatedImage animatedImageWithGIFData:[NSData dataWithContentsOfURL:url]];
    [self loadAnimatedImageWithURL:url completion:^(FLAnimatedImage *animatedImage) {
        self.GIFImgView.animatedImage  = animatedImage;
        [self.indicatorView stopAnimating];
        [self.indicatorView setHidden:YES];
    }];
    self.videoPlaceHolderImageView.clipsToBounds = YES;
    self.GIFImgView.clipsToBounds = YES;
}

-(void)stopPlayGif
{
    [self.indicatorView setHidden:YES];
    [self.indicatorView stopAnimating];
    self.GIFLabel.text = @"GIF";
    [self.GIFImgView stopAnimating];
}

- (void)loadAnimatedImageWithURL:(NSURL *const)url completion:(void (^)(FLAnimatedImage *animatedImage))completion
{
    NSString *const filename = url.lastPathComponent;
    NSString *const diskPath = [NSHomeDirectory() stringByAppendingPathComponent:filename];
    
    NSData * __block animatedImageData = [[NSFileManager defaultManager] contentsAtPath:diskPath];
    FLAnimatedImage * __block animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
    
    if (animatedImage) {
        if (completion) {
            completion(animatedImage);
        }
    } else {
        [[[NSURLSession sharedSession] dataTaskWithURL:url completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            animatedImageData = data;
            animatedImage = [[FLAnimatedImage alloc] initWithAnimatedGIFData:animatedImageData];
            
            if (animatedImage) {
                if (completion) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        completion(animatedImage);
                    });
                }
                [data writeToFile:diskPath atomically:YES];
            }
        }] resume];
    }
}
-(void) bindingVideoInfo
{
    self.likeButton.selected = self.video.liked;
    self.dislikeButton.selected = self.video.disliked;
    [self.likeButton setTitle:[@(self.video.like) stringValue] forState:UIControlStateNormal];
    [self.likeButton setTitle:[@(self.video.like) stringValue] forState:UIControlStateSelected];
    [self.dislikeButton setTitle:[@(self.video.dislike) stringValue] forState:UIControlStateNormal];
    [self.dislikeButton setTitle:[@(self.video.dislike) stringValue] forState:UIControlStateSelected];
    [self.numOfCommentButton setTitle:[@(self.video.comment) stringValue] forState:UIControlStateNormal];
    [self.numOfShareButton setTitle:[@(self.video.share) stringValue] forState:UIControlStateNormal];
}
#pragma mark - IBAction
- (IBAction)commentTapped:(id)sender {
    if (self.delegate) {
        [self.delegate commentVideoAtIndex:self.idxRow];
    }
}

- (IBAction)likeButtonDidTap:(UIButton *)sender
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGINED]) {
        
        sender.selected = !sender.selected;
        if (self.dislikeButton.selected == YES)
        {
            self.video.disliked = NO;
            self.video.dislike--;
            self.dislikeButton.selected = NO;
        }
        
        if (sender.selected == NO)
        {
            self.video.like--;
            self.video.liked = NO;
            [self bindingVideoInfo];
            if ([self.delegate respondsToSelector:@selector(GIFCellDidTapUnLikeUndislike:)])
            {
                [self.delegate GIFCellDidTapUnLikeUndislike:self];
            }
        }
        else
        {
            self.video.like++;
            self.video.liked = YES;
            [self bindingVideoInfo];
            
            if ([self.delegate respondsToSelector:@selector(GIFCellDidTapLike:)])
            {
                [self.delegate GIFCellDidTapLike:self];
            }
        }
        
    } else {
        if (self.delegate) {
            [self.delegate showLoginView];
        }
    }
}

- (IBAction)dislikeButtonDidTap:(UIButton *)sender
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGINED]) {
        sender.selected = !sender.selected;
        if (self.likeButton.selected == YES)
        {
            self.video.like--;
            self.video.liked = NO;
            self.likeButton.selected = NO;
        }
        
        if (sender.selected == NO)
        {
            self.video.disliked = NO;
            self.video.dislike--;
            [self bindingVideoInfo];
            
            if ([self.delegate respondsToSelector:@selector(GIFCellDidTapUnLikeUndislike:)])
            {
                [self.delegate GIFCellDidTapUnLikeUndislike:self];
            }
        }
        else
        {
            self.video.disliked = YES;
            self.video.dislike++;
            [self bindingVideoInfo];
            
            if ([self.delegate respondsToSelector:@selector(GIFCellDidTapDislike:)])
            {
                [self.delegate GIFCellDidTapDislike:self];
            }
        }
        
    }  else {
        if (self.delegate) {
            [self.delegate showLoginView];
        }
    }
    
}
- (IBAction)shareTapped:(id)sender {
    if (self.delegate) {
        [self.delegate shareVideoAtIndex:self.idxRow];
    }
}
@end
