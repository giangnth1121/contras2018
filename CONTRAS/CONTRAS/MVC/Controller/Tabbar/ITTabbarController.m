//
//  ITTabbarController.m
//  SpecTee
//
//  Created by Nguyen Ha Giang on 3/14/17.
//  Copyright © 2017 ITLancerApp. All rights reserved.
//

#import "ITTabbarController.h"
#import "BaseNavigationViewController.h"
//#import "NewVC.h"
#import "HomeVC.h"
#import "OrderVC.h"
#import "SettingVC.h"
#import "StoreVC.h"
#import "TeamVC.h"
#import "NewVC.h"
#import "ITTabbarView.h"

@interface ITTabbarController () <ITTabbarViewDelegate>
{
    BaseNavigationViewController *_newNav;
    BaseNavigationViewController *_storeNav;
    BaseNavigationViewController *_teamNav;
    BaseNavigationViewController *_settingNav;
    
    BOOL _isShowingTabbar;
}
@property (nonatomic, strong) ITTabbarView *tabbarView;
@end

@implementation ITTabbarController
- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupTabBarView];
    }
    return self;
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        [self setupTabBarView];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setupTabBarView];
    }
    return self;
}

- (void)setupTabBarView {
    _tabbarView = [[ITTabbarView alloc] init];
    _tabbarView.delegate = self;
    
    [self.tabBar addSubview:_tabbarView];
    
    for (UITabBarItem* button in self.tabBar.items) {
        button.imageInsets = UIEdgeInsetsMake(10000, 0, 0, 0);
        button.titlePositionAdjustment = UIOffsetMake(0, -100000);
        button.image = nil;
        button.selectedImage = nil;
    }
    _isShowingTabbar = YES;
}


- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    _tabbarView.frame = self.tabBar.bounds;
    _tabbarView.delegate = self;
    for (UIView *view in self.tabBar.subviews) {
        [view removeFromSuperview];
    }
    [self.tabBar addSubview:_tabbarView];
}


- (void)setTabBarViewHeight:(CGFloat)tabBarViewHeight {
    CGRect newFrame = self.tabBar.frame;
    newFrame.size.height = tabBarViewHeight;
    [self.tabBar setFrame:newFrame];
}

- (CGFloat)tabBarViewHeight {
    return self.tabBar.frame.size.height;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self initTabbar];
}

- (void)initTabbar {
    
    NSMutableArray *listViewControllers = [[NSMutableArray alloc] initWithCapacity:4];
    
    NewVC *newVC = [[NewVC alloc] initWithNibName:@"NewVC" bundle:nil];
    _newNav = [[BaseNavigationViewController alloc] initWithRootViewController:newVC];
    [listViewControllers addObject:_newNav];
    
    StoreVC *storeVC = [[StoreVC alloc] initWithNibName:@"StoreVC" bundle:nil];
    _storeNav = [[BaseNavigationViewController alloc] initWithRootViewController:storeVC];
    [listViewControllers addObject:_storeNav];

    TeamVC *teamVC = [[TeamVC alloc] initWithNibName:@"TeamVC" bundle:nil];
    _teamNav = [[BaseNavigationViewController alloc] initWithRootViewController:teamVC];
    [listViewControllers addObject:_teamNav];
    
    SettingVC *settingVC = [[SettingVC alloc] initWithNibName:@"SettingVC" bundle:nil];
    _settingNav = [[BaseNavigationViewController alloc] initWithRootViewController:settingVC];
    [listViewControllers addObject:_settingNav];
    
    self.viewControllers = listViewControllers;
}

#pragma mark - Tabbar
- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    [self.tabBar bringSubviewToFront:self.tabbarView];
}

- (BOOL)tabBar:(ITTabbarView *)tabBar shouldSelectItemAtIndex:(NSUInteger)index {
    UIViewController *viewControllerToSelect = self.viewControllers[index];
    
    BOOL shouldAskForPermission = [self.delegate respondsToSelector:@selector(tabBarController:shouldSelectViewController:)];
    
    BOOL selectionAllowed = YES;
    
    if (shouldAskForPermission) {
        selectionAllowed = [self.delegate tabBarController:self shouldSelectViewController:viewControllerToSelect];
    }
    
    return selectionAllowed;
}

- (void)tabBar:(ITTabbarView *)tabBar didSelectItemAtIndex:(NSUInteger)index {
    
    [self selectedTab:index];
    self.selectedViewController = self.viewControllers[index];
    
    if (index == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReloadDataNews" object:nil];
    } else {
        
    }

}

- (void)selectedTab:(TabName)tabSelected{
    self.selectedIndex = tabSelected;
    [self.tabbarView setSelectedTabBarItemIndex:tabSelected];
    [self.tabBar bringSubviewToFront:_tabbarView];
}
-(BOOL)isShowingTabbar{
    return _isShowingTabbar;
}
- (void)hideTabbar:(BOOL)isHide {
    if (isHide) {
        
        for (UIView *subview in [self.view subviews]) {
            
            if ([subview  isKindOfClass:[UITabBarItem class]] || [subview isKindOfClass:[UITabBar class]]) {
                [subview setHidden:YES];
            }
        }
        
    }else{
        
        for (UIView *subview in [self.view subviews]) {
            
            if ([subview  isKindOfClass:[UITabBarItem class]] || [subview isKindOfClass:[UITabBar class]]) {
                [subview setHidden:NO];
            }
        }
    }
    _isShowingTabbar = !isHide;
    
}
- (void)setNumOfNewUpdate:(NSInteger)num {
    [self.tabbarView setNumOfNewUpdate:num];
}
@end
