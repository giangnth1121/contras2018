//
//  UILabel+Html.m
//  TGS
//
//  Created by Nguyen Ha Giang on 3/2/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "UILabel+Html.h"

@implementation UILabel (Html)
- (void) setHtml: (NSString*) html
{
    html = [NSString stringWithFormat:@"<html><head><meta charset=\"UTF-8\"><style>body{font-family: '%@'; font-size:%fpx;}</style></head><body>%@</body></html>",@"roboto",self.font.pointSize,html];
    NSError *err = nil;
    
    NSMutableAttributedString *at = [[NSMutableAttributedString alloc] initWithData: [html dataUsingEncoding:NSUTF8StringEncoding] options: @{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes: nil error: &err];
    
    self.attributedText = at;
    if(err)
        NSLog(@"Unable to parse label text: %@", err);
}
@end
