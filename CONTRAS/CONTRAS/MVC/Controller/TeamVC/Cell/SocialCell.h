//
//  SocialCell.h
//  CONTRAS
//
//  Created by Giang Béo  on 3/1/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SocialCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@end
