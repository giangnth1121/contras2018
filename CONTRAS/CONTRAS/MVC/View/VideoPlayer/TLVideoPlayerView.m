//
//  TLVideoPlayerView.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "TLVideoPlayerView.h"
#import "TLVideoPlayerControlView.h"
#import "PSYouTubeExtractor.h"
#import "UIView+Utils.h"
#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import TwitterKit;
#import <FBSDKMessengerShareKit/FBSDKMessengerShareKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
//#import "APIErrorHandler.h"

@interface TLVideoPlayerView()<TLVideoPlayerControlViewDelegate,FBSDKSharingDelegate>
@property (nonatomic,strong) id timeObserver;
@property (nonatomic,strong) AVPlayerLayer *playerLayer;
@end

@implementation TLVideoPlayerView
{
    
}

-(void)layoutSubviews
{
    [super layoutSubviews];
    
    self.playerLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    [self addControlView];
    
    return self;
}

-(instancetype)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    [self addControlView];
    
    return self;
}

-(void) addControlView
{
    self.controlView = [TLVideoPlayerControlView loadViewWithNib];
    self.controlView.delegate = self;
    self.controlView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.controlView.shareView setHidden:YES];
    [self addSubview:self.controlView];
    
    [self.topAnchor constraintEqualToAnchor:self.controlView.topAnchor].active = YES;
    [self.bottomAnchor constraintEqualToAnchor:self.controlView.bottomAnchor].active = YES;
    [self.leftAnchor constraintEqualToAnchor:self.controlView.leftAnchor].active = YES;
    [self.rightAnchor constraintEqualToAnchor:self.controlView.rightAnchor].active = YES;
    
    [self resetControlViewState];
}

-(void) resetControlViewState
{
    self.controlView.alpha = 1;
    
    [self.controlView resetState];
}

-(void)setVideo:(VideoModel *)video
{
    _video = video;
    
    self.controlView.thumbnailURL = video.img;
    self.controlView.videoTitle = video.title;
    self.controlView.videoDuration = video.duration;
}

-(void) startVideoObserve
{
    __weak TLVideoPlayerView *weakSelf = self;
    self.timeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        AVPlayerItem *currentPlayerItem = weakSelf.player.currentItem;
        
        CGFloat currentTime = CMTimeGetSeconds(time);
        CGFloat endTime = CMTimeGetSeconds(currentPlayerItem.asset.duration);
        
        CGFloat progress = currentTime/endTime;
        weakSelf.controlView.progressView.progress = progress;
        weakSelf.controlView.timeSlider.value = currentTime;
        weakSelf.controlView.timeSlider.maximumValue = endTime;
        
        weakSelf.controlView.currentTimeLabel.text = [Utils getTimeStringFromTime:currentTime];
        weakSelf.controlView.durationTimeLabel.text = [Utils getTimeStringFromTime:endTime];
        
        if (currentTime != 0)
        {
            //NSLog(@"Set seek time: %d",(int)currentTime);
            weakSelf.video.seekTime = currentTime;
        }
        //        [DC.currentVideoPlayTimeStore setObject:@(currentTime) forKey:@(weakSelf.video.videoId)];
    }];
    
    [self.player.currentItem addObserver:self forKeyPath:@"status" options:NSKeyValueObservingOptionNew context:nil];
    [self.player.currentItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionNew context:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerItemDidEndNotification:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.player.currentItem];
}

#pragma mark - Function
-(void)play
{
    [self.controlView.shareView setHidden:YES];

    
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];
    
    if (self.video.directLink.length)
    {
        [self preparePlay];
    }
    else if(self.video.type == VideoTypeYt)
    {
        NSString *ytVideoURL = self.video.clip;
        NSString* encodeVideoURL = [ytVideoURL stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];

        [self.controlView setControlState:ControlStateLoading];
        [PSYouTubeExtractor extractorForYouTubeURL:[NSURL URLWithString:encodeVideoURL] success:^(NSURL *URL) {
            self.video.directLink = URL.absoluteString;
            
            if (self.controlView.controlState == ControlStateLoading)
            {
                [self preparePlay];
            }
        } failure:^(NSError *error) {
            [self.controlView setControlState:ControlStateIdle];
        }];
    }
}

-(void) preparePlay
{
    if (![[(AVURLAsset *)self.player.currentItem.asset URL].absoluteString isEqualToString:self.video.directLink])
    {
        [self stop];
    }
    
    if (!self.player)
    {
        NSString *directLink = self.video.directLink;
        NSString* encodedirectLink = [directLink stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
        
        AVPlayerItem *playerItem = [[AVPlayerItem alloc] initWithURL:[NSURL URLWithString:encodedirectLink]];
        self.player = [AVPlayer playerWithPlayerItem:playerItem];
        self.player.muted = [[NSUserDefaults standardUserDefaults] boolForKey:@"mute"];
    }
    
    if (!self.playerLayer)
    {
        self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
        self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspect;
        self.playerLayer.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        [self.layer insertSublayer:self.playerLayer atIndex:0];
        
        [self startVideoObserve];
    }
    
    if (self.player.status == AVPlayerStatusReadyToPlay)
    {
        [self.controlView setControlState:ControlStatePlaying];
        [self.player play];
    }
    else
    {
        [self.controlView setControlState:ControlStateLoading];
    }
}

-(void)pause
{
    [self.player pause];
    [self.controlView setControlState:ControlStatePause];
}

-(void)stop
{
    [self.player pause];
    [self stopHereOnly];
    self.player = nil;
}

-(void)stopHereOnly
{
    if (self.playerLayer)
    {
        @try
        {
            [self.player.currentItem removeObserver:self forKeyPath:@"status" context:nil];
            [self.player.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty" context:nil];
        }
        @catch (id ex)
        {
            NSLog(@"Exception status");
        }
        
        [self.playerLayer removeFromSuperlayer];
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        [self.player.currentItem.asset cancelLoading];
        [self.player.currentItem cancelPendingSeeks];
        
        [self.player removeTimeObserver:self.timeObserver];
        self.timeObserver = nil;
        self.playerLayer = nil;
    }
    
    [self resetControlViewState];
}

-(void)seekTimeTo:(NSTimeInterval)time
{
    [self.player pause];
    [self.player seekToTime:CMTimeMake(time*100, 100) completionHandler:^(BOOL finished) {
        if (finished)
        {
            [self.controlView setControlState:ControlStateShowSeekControl];
            [self.player play];
        }
        else
        {
            if (self.controlView.controlState != ControlStateLoadingBuffer)
            {
                [self.controlView setControlState:ControlStateLoadingBuffer];
            }
        }
    }];
}

#pragma mark - Notification & observer
-(void) playerItemDidEndNotification:(NSNotification *)notification
{
    self.video.seekTime = 0;
    [self.player pause];
    [self.player seekToTime:kCMTimeZero];
    [self.controlView setControlState:ControlStateIdle];
    
    if ([self.delegate respondsToSelector:@selector(videoPlayerDidEndVideo:)])
    {
        [self.delegate videoPlayerDidEndVideo:self];
    }
    
    [self.controlView.shareView setHidden:NO];
}

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    if ([keyPath isEqualToString:@"status"])
    {
        AVPlayerItemStatus status = self.player.currentItem.status;
        
        if ([self.delegate respondsToSelector:@selector(videoPlayer:didChangePlayerItemStatus:)])
        {
            [self.delegate videoPlayer:self didChangePlayerItemStatus:status];
        }
        
        if (status == AVPlayerStatusReadyToPlay)
        {
            if (self.video.seekTime == 0)
            {
                [self.controlView setControlState:ControlStatePlaying];
                NSLog(@"Playing %@",self.video.clip);
                [self.player play];
            }
            else
            {
                [self.player pause];
                [self.player seekToTime:CMTimeMake(self.video.seekTime*100, 100) completionHandler:^(BOOL finished) {
                    if (finished)
                    {
                        [self.controlView setControlState:ControlStatePlaying];
                        [self.player play];
                    }
                    else
                    {
                        NSLog(@"Seek time fail");
                    }
                }];
            }
        }
    }
    
    if ([keyPath isEqualToString:@"playbackBufferEmpty"])
    {
        if (self.player.currentItem.playbackBufferEmpty)
        {
            [self.controlView setControlState:ControlStateLoadingBuffer];
        }
        else if (self.player.currentItem.playbackLikelyToKeepUp == YES && self.controlView.controlState == ControlStateLoadingBuffer)
        {
            NSLog(@"Continue Buffering: %d",self.player.currentItem.playbackBufferEmpty);
            [self.controlView setControlState:ControlStatePlaying];
            [self.player play];
        }
    }
}

#pragma mark - View cycle
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (self.controlView.controlState == ControlStatePlaying)
    {
        self.controlView.controlState = ControlStateShowSeekControl;
        
        return;
    }
    
    if (self.controlView.controlState == ControlStateShowSeekControl)
    {
        self.controlView.controlState = ControlStatePlaying;
        return;
    }
}

#pragma mark - Control View delegate
-(void)TLVideoPlayerControlViewDidTapPlay:(TLVideoPlayerControlView *)controller
{
    if ([self.delegate respondsToSelector:@selector(videoPlayerDidTapPlay:)])
    {
        [self.delegate videoPlayerDidTapPlay:self];
    }

    [self play];
}

-(void)TLVideoPlayerControlViewDidTapPause:(TLVideoPlayerControlView *)controller
{
    [self pause];
}

-(void)TLVideoPlayerControlViewDidTapFullscreen:(TLVideoPlayerControlView *)controller
{
    if ([self.delegate respondsToSelector:@selector(videoPlayerDidTapFullScreen:)])
    {
        [self.delegate videoPlayerDidTapFullScreen:self];
    }
}

-(void)TLVideoPlayerControlView:(TLVideoPlayerControlView *)controller didSeekToValue:(NSInteger)value
{
    [self seekTimeTo:value];
}

-(void) videoShareFb:(TLVideoPlayerControlView *)controller {
    [self shareVideoToFacebook];
}
-(void) videoShareTw:(TLVideoPlayerControlView *)controller {
    [self shareVideoToTwitter];
}

-(void) videoShareMore:(TLVideoPlayerControlView *)controller {
    [self shareMore];
}
-(void) videoReplay:(TLVideoPlayerControlView *)controller {
    [self play];
}
- (void)shareVideoToFacebook {
    
    NSString *url = StringIsNullOrEmpty( self.video.shareLink) ? self.video.clip : self.video.shareLink;
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:url];;
    
    [FBSDKShareDialog showFromViewController:self.videoController
                                 withContent:content
                                    delegate:self];

}
- (void)shareVideoToTwitter {
    NSString *url = StringIsNullOrEmpty( self.video.shareLink) ? self.video.clip : self.video.shareLink;
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
        SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        [controller addURL:[NSURL URLWithString:url]];
        [controller setInitialText:[NSString stringWithFormat:@"Watch %@ #TopShare",self.video.title]];
        [controller setCompletionHandler:^(SLComposeViewControllerResult result)
         {
             if (result == SLComposeViewControllerResultCancelled) {
                 
                 
             } else if (result == SLComposeViewControllerResultDone) {
                 [self callShareAPI];
             }
         }];
        [self.videoController presentViewController:controller animated:YES completion:nil];
    } else {
        NSString *message = @"There are no Twitter accounts configured. You can add or create a Twitter account in Settings.";
        [[Global share] showAlertWithTitle:@"No Twitter Account"
                                   message:message
                               titleButton:@"Setting"
                                    action:^{
                                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:UIApplicationOpenSettingsURLString]];
                                    } inView:self.videoController];
    }

}
- (void)shareMore {
    
    NSString *url = StringIsNullOrEmpty( self.video.shareLink) ? self.video.clip : self.video.shareLink;
    NSString * title = [NSString stringWithFormat:@"Watch %@ #TopShare %@",self.video.title, url];
    
    NSArray *dataToShare = @[title];
    UIActivityViewController* activityViewController =[[UIActivityViewController alloc] initWithActivityItems:dataToShare applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypeAirDrop];
    [activityViewController setCompletionWithItemsHandler:^(UIActivityType  _Nullable activityType, BOOL completed, NSArray * _Nullable returnedItems, NSError * _Nullable activityError) {
        if ( completed )
        {
            [self callShareAPI];
        }
    }];
    [self.videoController  presentViewController:activityViewController animated:YES completion:^{}];
}
- (void)callShareAPI {
//    [APIController takeActionWithNewID:self.video.videoId
//                            actionType:3
//                       completeHandler:^(NSDictionary *dict) {
//                           [Utils showAlertWithTitle:@"Success !" andMessage:nil];
//    } failure:^(APIResult *apiResult) {
//         NSString *message = [APIErrorHandler messageWithAPIResult:apiResult];
//        [Utils showAlertWithTitle:nil andMessage:message];
//    }];
}
#pragma mark - FBSDKSharingDelegate
- (void)sharer:(id<FBSDKSharing>)sharer didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"completed");
    [self callShareAPI];
}

- (void)sharer:(id<FBSDKSharing>)sharer didFailWithError:(NSError *)error {
    NSLog(@"fail %@",error.description);
}

- (void)sharerDidCancel:(id<FBSDKSharing>)sharer {
    NSLog(@"cancel");
}
@end
