//
//  ManageAccountCell.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol ManageAccountCellDelegate <NSObject>

@optional
- (void)enableLoginSocialAccount:(NSInteger)index;
@end

@interface ManageAccountCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *detailLabel;
@property (nonatomic, weak) IBOutlet UILabel *centerLabel;
@property (nonatomic, weak) IBOutlet UIImageView *avaImg;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImg;
@property (nonatomic, weak) IBOutlet UISwitch *switchButton;
@property (nonatomic, assign) NSInteger indexRow;
@property (nonatomic, weak) id <ManageAccountCellDelegate> delegate;

- (void)resetCell;
@end
