//
//  LoginSelectViewController.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/17/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "BaseViewController.h"
@protocol LoginSelectVCDelegate <NSObject>
- (void)reloadWhenLoginSuccess;
//- (void)loginFacebook;
//- (void)loginGmail;
//- (void)loginTwitter;
@end

@interface LoginSelectViewController : BaseViewController
// Property

// Outlet
@property (weak, nonatomic) IBOutlet UIView *contentContainerView;
// IBAction
- (IBAction)termButtonDidTap:(id)sender;
- (IBAction)privacyPolicyButtonDidTap:(id)sender;
- (IBAction)loginFacebookButtonDidTap:(id)sender;
- (IBAction)loginTwitterButtonDidTap:(id)sender;
- (IBAction)loginGoogleButtonDidTap:(id)sender;
- (IBAction)signInWithEmailButtonDidTap:(id)sender;
- (IBAction)subcribeButtonDidTap:(id)sender;

@property (weak, nonatomic) id <LoginSelectVCDelegate> delegate;

// Public function


@end
