//
//  SettingVC.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "SettingVC.h"
#import "SettingCell.h"
#import "UserTableViewCell.h"
#import "LoginTableViewCell.h"

#import "ManageAccountVC.h"
#import "MyPostsVC.h"
#import "ListNotifyVC.h"
#import "SettingAppVC.h"
#import "FeedbackVC.h"
#import "TermAndPolicyVC.h"
#import "ListNotifyVC.h"
#import "UIImage+AFNetworking.h"

#import <Google/SignIn.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import TwitterKit;
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AppDelegate.h"

@interface SettingVC () <UITableViewDataSource, UITableViewDelegate,LoginTableViewCellDelegate,GIDSignInUIDelegate,GIDSignInDelegate,MFMailComposeViewControllerDelegate>
{
    NSMutableArray *_titleArray;
    NSMutableArray *_imagesArray;
}
@property (nonatomic, strong) IBOutlet UITableView *settingTableView;
@end

@implementation SettingVC

-(BOOL) prefersStatusBarHidden{
    return NO;
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
    [[AppDelegate shareAppDelegate] hideTabbar:NO];
    [self reloadData];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)configUI{
    self.settingTableView.dataSource = self;
    self.settingTableView.delegate = self;
    [self.settingTableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
    [self.settingTableView registerNib:[UINib nibWithNibName:@"UserTableViewCell" bundle:nil] forCellReuseIdentifier:@"UserTableViewCell"];
    [self.settingTableView registerNib:[UINib nibWithNibName:@"LoginTableViewCell" bundle:nil] forCellReuseIdentifier:@"LoginTableViewCell"];
    
    self.settingTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    _titleArray     = [@[
                         @"Setting",
                         @"Feedback"] mutableCopy];
    _imagesArray    = [@[
                         @"ic_setting",
                         @"ic_feedback"] mutableCopy];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)reloadData {
    [self.settingTableView reloadData];
}

#pragma mark - UITableView DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
    
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return (section == 0) ? 1: [_titleArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:IS_LOGINED]) {
            UserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserTableViewCell"];
            if (cell == nil ){
                cell = [[UserTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"UserTableViewCell"];
            }
            NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USERNAME] ? [[NSUserDefaults standardUserDefaults] objectForKey:KEY_USERNAME]:@"";
            NSString *linkAvatar = [[NSUserDefaults standardUserDefaults] objectForKey:KEY_LINKAVATAR];
            cell.userNameLabel.text = userName;
            [cell.avatarImg sd_setImageWithURL:[NSURL URLWithString:linkAvatar] placeholderImage:[UIImage imageNamed:@"ic_profile"]];
            return cell;
          
        } else {
            LoginTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LoginTableViewCell"];
            if (cell == nil ){
                cell = [[LoginTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"LoginTableViewCell"];
            }
            cell.delegate = self;
            return cell;
        }
       
    } else {
        SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
        if (cell == nil ){
            cell = [[SettingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"SettingCell"];
        }
        [cell.iconImg setImage:[UIImage imageNamed:_imagesArray[indexPath.row]]];
        cell.titleLabel.text = _titleArray[indexPath.row];
        return cell;
    }
   
}
#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.section == 0 ) ? 180: 45;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ManageAccountVC *manageAccVC = [[ManageAccountVC alloc] initWithNibName:@"ManageAccountVC" bundle:nil];
        [self.navigationController pushViewController:manageAccVC animated:YES];
    } else {
        switch (indexPath.row) {
//            case 0:
//            {
//                MyPostsVC *mypostVC = [[MyPostsVC alloc] initWithNibName:@"MyPostsVC" bundle:nil];
//                [self.navigationController pushViewController:mypostVC animated:YES];
//            }
//                break;
            case 0:
            {
                SettingAppVC *settingApp = [[SettingAppVC alloc] initWithNibName:@"SettingAppVC" bundle:nil];
                [self.navigationController pushViewController:settingApp animated:YES];
            }
                break;

            case 1:
            {
                [self feedbackViaEmail];
            }
                break;

                break;
            default:
                break;
        }
    }
   
}
#pragma mark - LoginTableViewCell Delegate

- (void)loginFacebook {
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    [login logInWithReadPermissions:@[@"email"] fromViewController:self
                            handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
                                if (error) {
                                    // Process error
                                    NSLog(@"error %@",error);
                                } else if (result.isCancelled) {
                                    // Handle cancellations
                                    NSLog(@"Cancelled");
                                } else {
                                    if ([result.grantedPermissions containsObject:@"email"]) {
                                        // Do work
                                        NSLog(@"%@",result);
                                        [self fetchUserInfoFromFB];
                                    }
                                }
                            }];
}
-(void)fetchUserInfoFromFB
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        NSLog(@"Token is available : %@",[[FBSDKAccessToken currentAccessToken]tokenString]);
        NSString *social_token = [[FBSDKAccessToken currentAccessToken]tokenString];
        
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me"
                                           parameters:@{@"fields": @"id,name,email,picture.type(large),location,birthday"}]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
             if (!error)
             {
//                 NSLog(@"resultis:%@",result);
//                 email = "hagiangnguyen.iosdev@gmail.com";
//                 id = 360482221064105;
//                 name = "Nguy\U1ec5n H\U00e0 Giang";
//                 picture =     {
//                     data =         {
//                         "is_silhouette" = 0;
//                         url = "https://scontent.xx.fbcdn.net/v/t1.0-1/p200x200/13912741_121601268285536_2258178222197042275_n.jpg?oh=d73f785231b25a2f858ecc924afdbb57&oe=5A732919";
//                     };
//                 };
                 
                 NSDictionary *dictUser = (NSDictionary *)result;
                 UserModel *userModel  =  [[UserModel alloc] init];// = [[UserModel alloc] initWithDictionary:result error:nil];
                 userModel.email = dictUser[@"email"];
                 userModel.username = dictUser [@"name"];
                 userModel.avatar = [NSString stringWithFormat:@"%@",result[@"picture"][@"data"][@"url"]];
                
                 [self socialLoginWithUser:userModel
                               loginType:kLoginFacebook
                              social_token:social_token];
             }
             else
             {
                 NSLog(@"Error %@",error);
             }
         }];
    }
}

- (void)loginTwitter {
    // Objective-C
    // Objective-C
    [[Twitter sharedInstance] logInWithCompletion:^(TWTRSession *session, NSError *error) {
        if (session) {
            NSLog(@"signed in as %@", [session userName]);
        //    NSString *authToken          = [NSString stringWithFormat:@"%@",[session authToken]];
          //  NSString *authTokenSecret    = [NSString stringWithFormat:@"%@",[session authTokenSecret]];
            NSString *username           = [NSString stringWithFormat:@"%@",[session userName]];
          //  NSString *userID             = [NSString stringWithFormat:@"%@",[session userID]];
            UserModel *userModel = [[UserModel alloc] init];
            userModel.username = username;
            userModel.userID = [session userID];

            
            [self socialLoginWithUser:userModel
                            loginType:kLoginTwitter
                         social_token:@""];
            [self.settingTableView reloadData];
            
        } else {
            NSLog(@"error: %@", [error localizedDescription]);
        }
    }];
}
- (void)loginGmail {
    [GIDSignIn sharedInstance].uiDelegate = self;
    [GIDSignIn sharedInstance].delegate   = self;
    [[GIDSignIn sharedInstance] signIn];
  //  [GIDSignIn sharedInstance].shouldFetchGoogleUserEmail = YES;
    [GIDSignIn sharedInstance].scopes = @[ @"profile" ];
}
- (void)socialLoginWithUser:(UserModel *)user
                loginType:(loginType)loginType
               social_token:(NSString *)social_token{
//      [GiFHUD setGifWithImageName:@"ic_loading.gif"];
//    [GiFHUD show];
//    [APIController signInWithUser:user
//                        loginType:loginType
//                  completeHandler:^(UserModel *userLogin) {
//                      NSLog(@"%@",userLogin);
//                      user.userID = userLogin.userID;
//                     [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginSuccess object:nil];
//                      [self saveDataUser:user
//                               loginType:loginType];
//                      [self.settingTableView reloadData];
//                      [GiFHUD dismiss];
//    } failure:^(APIResult *apiResult) {
//
//        [GiFHUD dismiss];
//        [self handleAPIError:apiResult];
//    }];
}
#pragma mark - GIDSignInDelegate
- (void)signIn:(GIDSignIn *)signIn
didSignInForUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations on signed in user here.
    NSString *userId = user.userID;                  // For client-side use only!
    NSString *idToken = user.authentication.idToken; // Safe to send to the server
    NSString *email = user.profile.email;
    
    NSString *fullName = user.profile.name;
    //    NSString *givenName = user.profile.givenName;
    //    NSString *familyName = user.profile.familyName;
    NSLog(@"LOGIN GOOGLE:%@",user);
    
    NSURL *url = (user.profile.hasImage) ?  [user.profile imageURLWithDimension:100] : [NSURL URLWithString:@""];
    NSString *avatar = url.absoluteString;
    NSLog(@"url : %@",url);
    UserModel *userModel = [[UserModel alloc] init];
    userModel.email         = email;
    userModel.userID            = [userId integerValue];
    userModel.username          = fullName;
    userModel.avatar        = avatar;

    [self socialLoginWithUser:userModel
                  loginType:kLoginGoogle
                 social_token:idToken];
    // ...
}

- (void)signIn:(GIDSignIn *)signIn
didDisconnectWithUser:(GIDGoogleUser *)user
     withError:(NSError *)error {
    // Perform any operations when the user disconnects from app here.
    // ...
}
#pragma mark - Save Info User
- (void)saveDataUser:(UserModel *)userModel loginType:(loginType)loginType {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_LOGINED];
   // [AppDelegate shareAppDelegate].loginType = loginType;
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%ld",userModel.userID] forKey:KEY_USER_ID];
    if (userModel.avatar)[[NSUserDefaults standardUserDefaults] setObject: userModel.avatar forKey:KEY_LINKAVATAR];
    [[NSUserDefaults standardUserDefaults] setObject:userModel.username forKey:KEY_USERNAME];
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%i",[[AppDelegate shareAppDelegate] loginType]] forKey:KEY_LOGINTYPE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void)feedbackViaEmail {
    // From within your active view controller
    if([MFMailComposeViewController canSendMail]) {
        MFMailComposeViewController *mailVC = [[MFMailComposeViewController alloc] init];
        mailVC.mailComposeDelegate = self;
        
        [mailVC setSubject:@"Feedback"];
        [mailVC setToRecipients:[NSArray arrayWithObject:@"info@topshare.live"]];
        [mailVC setMessageBody:@"#TopShare" isHTML:NO];
        [self presentViewController:mailVC animated:YES completion:nil];

    } else {
      //  [Utils showAlertWithTitle:@"Error" andMessage:@"Can not send emal right now !"];
    }
}

#pragma mark - MFMailComposeViewController Delegate
// Then implement the delegate method
- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    [self dismissModalViewControllerAnimated:YES];
}
@end
