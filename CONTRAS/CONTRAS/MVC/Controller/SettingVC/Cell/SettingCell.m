//
//  SettingCell.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "SettingCell.h"

@implementation SettingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)resetCell {
    self.titleLabel.text = @"";
    self.contentLabel.text = @"";
    self.nameLabel.text = @"";
    [self.arrowImg setHidden:YES];
    [self.iconImg setHidden:YES];
    [self.checkBoxImg setHidden:YES];
    
}
@end
