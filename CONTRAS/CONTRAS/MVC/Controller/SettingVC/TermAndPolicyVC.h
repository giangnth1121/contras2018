//
//  TermAndPolicyVC.h
//  TopShare
//
//  Created by Giang Béo  on 11/30/17.
//  Copyright © 2017 Retech. All rights reserved.
//

#import "BaseViewController.h"

@interface TermAndPolicyVC : BaseViewController
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (nonatomic) BOOL isTerm;
@end
