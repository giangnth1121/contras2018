//
//  DetailVC.h
//  SACH
//
//  Created by Giang Béo  on 1/8/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "BaseViewController.h"

@interface DetailVC : BaseViewController

@property (nonatomic) NSDictionary *infoDict;
@end
