//
//  ShadowView.m
//  Assignment
//
//  Created by Nguyen Ha Giang on 9/15/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "ShadowView.h"

@implementation ShadowView


- (void)drawRect:(CGRect)rect {
    if (self.shadow)
    {
        self.layer.shadowOffset = self.shadowOffset;
        self.layer.shadowColor = [UIColor grayColor].CGColor;
        self.layer.shadowRadius = 2;
        self.layer.shadowOpacity = 0.5;
    }
}

@end
