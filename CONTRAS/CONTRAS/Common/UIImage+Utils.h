//
//  UIImage+Utils.h
//  ITLancer
//
//  Created by Nguyen Ha Giang  on 4/17/17.
//  Copyright © 2017 ITLancerApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Utils)

- (UIImage *)imageMaskedWithColor:(UIColor *)maskColor;
@end
