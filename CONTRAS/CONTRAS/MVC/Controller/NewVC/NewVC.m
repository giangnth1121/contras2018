//
//  NewVC.m
//  CONTRAS
//
//  Created by Giang Béo  on 2/28/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "NewVC.h"
#import "NewCell.h"
#import "HomeTableViewCell.h"
#import "GifImageCell.h"
@interface NewVC ()

@end

@implementation NewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    
    _newsTableView.dataSource   = self;
    _newsTableView.delegate     = self;
    [_newsTableView registerNib:[UINib nibWithNibName:@"NewCell" bundle:nil] forCellReuseIdentifier:@"NewCell"];
    [_newsTableView registerNib:[UINib nibWithNibName:@"HomeTableViewCell" bundle:nil] forCellReuseIdentifier:@"HomeTableViewCell"];
    [_newsTableView registerNib:[UINib nibWithNibName:@"GifImageCell" bundle:nil] forCellReuseIdentifier:@"GifImageCell"];
    _newsTableView.estimatedRowHeight = 200;
    _newsTableView.rowHeight = UITableViewAutomaticDimension;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        NewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewCell" forIndexPath:indexPath];
        return cell;
    } else  if (indexPath.row == 1)  {
        HomeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HomeTableViewCell" forIndexPath:indexPath];
        return cell;
    } else {
        GifImageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GifImageCell" forIndexPath:indexPath];
        return cell;
    }
 
}


@end
