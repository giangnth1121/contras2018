//
//  UIView+Utils.h
//  TGS
//
//  Created by Nguyen Ha Giang on 2/27/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Utils)
- (NSMutableArray*) allSubViews;
+(UIView *)loadViewWithNib;
@end
