//
//  SettingCell.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UIImageView *iconImg;
@property (nonatomic, weak) IBOutlet UIImageView *arrowImg;
@property (nonatomic, weak) IBOutlet UIImageView *checkBoxImg;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *nameLabel;
@property (nonatomic, weak) IBOutlet UILabel *contentLabel;

- (void)resetCell;
@end
