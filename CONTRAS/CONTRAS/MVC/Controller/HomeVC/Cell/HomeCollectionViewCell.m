//
//  HomeCollectionViewCell.m
//  SACH
//
//  Created by Giang Béo  on 12/25/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "HomeCollectionViewCell.h"

@implementation HomeCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
- (void)setContentCell:(NSDictionary *)dict {
    self.iconImg.clipsToBounds = YES;
    self.iconImg.layer.cornerRadius = 5.0f;
    self.iconImg.layer.masksToBounds = YES;
    self.iconImg.image = [UIImage imageNamed:dict[@"image"]];
    self.titleLabel.text = [dict[@"title"] uppercaseString];
    self.priceLabel.text = [NSString stringWithFormat:@"%@", [Utils formatCurrency:[dict[@"price"] integerValue]]];
//    self.priceLabel.text = [NSString stringWithFormat:@"%@đ/1", [Utils formatCurrency:[dict[@"price"] integerValue]] ,dict[@"value"]];
}
@end
