//
//  BaseModel.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseModel : NSObject

+(NSArray *) parseWithListDictionary:(NSArray *)list;
+(instancetype) parseWithDictionary:(NSDictionary *)dict;

@end
