//
//  BaseNavigationViewController.h
//  TopShare
//
//  Created by ThanhVu on 7/19/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface BaseBarButtonItem:UIBarButtonItem{
    UIView*   _parentView;
    UIButton* _button;
}
-(id) initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style tintColor:(UIColor*) tintColor target:(id)target action:(SEL)action;
-(id) initWithImageNamed:(NSString *)imageNamed target:(id)target action:(SEL)action;
-(void)setButtonVerticalPositionAdjustment:(UIOffset)adjustment forBarMetrics:(UIBarMetrics)barMetrics;
@end

@interface BaseNavigationViewController : UINavigationController


@end


@interface UINavigationController(AN)
- (void)setLeftNavigationButtonWithTitle:(NSString *)title action:(SEL)selector;
- (void)setLeftNavigationButtonImage:(NSString*)imageName action:(SEL)selector;
- (void)setRightNavigationButtonWithTitle:(NSString *)title action:(SEL)selector;
- (void)setRightNavigationButtonImage:(NSString*)imageName action:(SEL)selector;
- (void)setLeftNavigationButtonWithTitle:(NSString *)title tintColor:(UIColor*)tintColor action:(SEL)selector ;
- (void)setRightNavigationButtonWithTitle:(NSString *)title tintColor:(UIColor*)tintColor action:(SEL)selector;
- (void)setRightNavigationButtonWithTitle:(NSString *)title tintColor:(UIColor*)tintColor font:(UIFont*) font action:(SEL)selector ;
@end
