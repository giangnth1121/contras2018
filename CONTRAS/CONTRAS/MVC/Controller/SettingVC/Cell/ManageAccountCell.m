//
//  ManageAccountCell.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "ManageAccountCell.h"

@implementation ManageAccountCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.avaImg.layer.cornerRadius      = self.avaImg.frame.size.width/2;
    self.avaImg.layer.masksToBounds     = YES;
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)resetCell {
    [self.switchButton setHidden:YES];
    [self.arrowImg setHidden:YES];
    self.titleLabel.text = @"";
    self.detailLabel.text = @"";
    self.centerLabel.text = @"";
    [self.avaImg setHidden:YES];
}
- (IBAction)enableLoginSocial:(id)sender {
    if (self.delegate) {
        [self.delegate enableLoginSocialAccount:self.indexRow];
    }
}

@end
