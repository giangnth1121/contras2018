//
//  UIImage+Extensions.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Extensions)
-(NSString *)base64;
-(instancetype) initWithBase64String:(NSString *)base64String;
+(UIImage *) imageWithBase64String:(NSString *)base64String;
@end
