//
//  DetailVC.m
//  SACH
//
//  Created by Giang Béo  on 1/8/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "DetailVC.h"

@interface DetailVC ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@end

@implementation DetailVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    // Do any additional setup after loading the view from its nib.
}

- (void)configUI {
    self.title = [self.infoDict[@"title"] uppercaseString];
    [self setBackNavigationButton];
    self.iconImg.clipsToBounds = YES;
    self.iconImg.image = [UIImage imageNamed:self.infoDict[@"image"]];
//    self.titleLabel.text = [dict[@"title"] uppercaseString];
    self.priceLabel.text = [NSString stringWithFormat:@"%@", [Utils formatCurrency:[self.infoDict[@"price"] integerValue]] ];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
