//
//  ProductCell.m
//  CONTRAS
//
//  Created by Giang Béo  on 3/1/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgView.layer.cornerRadius = 5.0f;
    self.imgView.layer.masksToBounds = YES;
    self.lbPrice.textColor = [UIColor purpleColor];
    self.lbTotalPrice.textColor = [UIColor purpleColor];
    self.lbTotalPrice.font = [UIFont boldSystemFontOfSize:15];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)setContentOrderCell:(NSDictionary *)dict {
    [self.imgView setImage:[UIImage imageNamed:dict[@"image"]]];
    self.lbTitle.text = dict[@"title"];
    // [Utils formatCurrency:total]
    self.lbNumber.text = [NSString stringWithFormat:@"x%@",dict[@"number"]];
    NSInteger price = [dict[@"price"] integerValue];
    NSInteger number = [dict[@"number"] integerValue];
    self.lbPrice.text = [NSString stringWithFormat:@"%@", [Utils formatCurrency:[dict[@"price"] integerValue]] ];
    self.lbTotalPrice.text = [NSString stringWithFormat:@"%@",[Utils formatCurrency:price*number]];
}
@end
