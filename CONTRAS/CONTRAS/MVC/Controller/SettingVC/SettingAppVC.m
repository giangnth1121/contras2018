//
//  SettingAppVC.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "SettingAppVC.h"
#import "SettingVideoAutoplayVC.h"
#import "ManageAccountCell.h"
#import "SettingCell.h"
#import "TermAndPolicyVC.h"

@interface SettingAppVC () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *_titleArray;
}
@end

@implementation SettingAppVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:YES];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}
- (void)configUI {
    self.title = @"Settings";
    [self setBackNavigationButton];
    self.settingAppTableView.dataSource = self;
     self.settingAppTableView.delegate = self;
    [self.settingAppTableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
    self.settingAppTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _titleArray = @[
                    @"Rate Us",
                    @"Terms Of Service",
                    @"Privacy Policy",
                    @"Version"];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [_titleArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
    [cell resetCell];
    cell.nameLabel.text = _titleArray[indexPath.row];
   // cell.contentLabel.text = (indexPath.row == _titleArray.count - 1) ? [Utils getAppVersion]:@"";
    return cell;
    
}
#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  45;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
//        case 0://Autoplay
//        {
//            SettingVideoAutoplayVC *autoPlayVC = [[SettingVideoAutoplayVC alloc] init];
//            [self.navigationController pushViewController:autoPlayVC animated:YES];
//        }
//            break;
        case 0://Rate Us
        {
            // Rate Us
        }
            break;
        case 1:// Term
        {
            TermAndPolicyVC *termVC = [[TermAndPolicyVC alloc] init];
            termVC.isTerm = YES;
            [self.navigationController pushViewController:termVC animated:YES];
            
        }
            break;
        case 2:// Policy
        {
            TermAndPolicyVC *policyVC = [[TermAndPolicyVC alloc] init];
            policyVC.isTerm = NO;
            [self.navigationController pushViewController:policyVC animated:YES];
        }
            break;
        default:
            break;
    }
}
@end
