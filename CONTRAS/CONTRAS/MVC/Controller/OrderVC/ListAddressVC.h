//
//  ListAddressVC.h
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface ListAddressVC : BaseViewController

@property (nonatomic, strong)  NSMutableArray *ordersArray;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@end
