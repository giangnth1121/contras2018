//
//  UIColor+Ext.h
//  newwindsoft
//
//  Created by Vu Hoang Minh on 5/28/13.
//  Copyright (c) 2013 newwindsoft . All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Ext)

+ (UIColor *)colorWithHex:(NSString *)colorHex alpha:(CGFloat)alpha;
+ (UIColor *)kColorButtonShadowYellow;
+ (UIColor *)kColorButtonShadowBlue;
+ (UIColor *)kColorLineCell;
+ (UIColor *)kColorDefaultBlue;
+ (UIColor *)kColorDefaultAvatar;
+ (UIColor *)kBackgroundColorGray;
+ (UIColor *)kBackgroundColorBlue ;
+ (UIColor *)warmGrey;
+ (UIColor*)kColorBackgroundDisableButton;
+ (UIColor*)kColorButtonShadowGray;
+ (UIColor*)buttonTitleColorBlue;

@end
