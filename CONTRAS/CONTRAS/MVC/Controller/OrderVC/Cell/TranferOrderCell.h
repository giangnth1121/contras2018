//
//  TranferOrderCell.h
//  CONTRAS
//
//  Created by Giang Béo  on 3/1/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TranferOrderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbCost;
@property (weak, nonatomic) IBOutlet UILabel *lbNote;

@end
