//
//  SettingVideoAutoplayVC.m
//  TopShare
//
//  Created by Giang Béo  on 11/30/17.
//  Copyright © 2017 Retech. All rights reserved.
//

#import "SettingVideoAutoplayVC.h"

#import "ManageAccountCell.h"
#import "SettingCell.h"

@interface SettingVideoAutoplayVC () <UITableViewDataSource, UITableViewDelegate>
{
    NSArray *_titleArray;
    NSMutableArray *_selectedArray;
}
@end

@implementation SettingVideoAutoplayVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:YES];
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
}
- (void)configUI {
    
    self.title = @"Video Autoplay";
    [self setBackNavigationButton];
    self.settingAppTableView.dataSource = self;
    self.settingAppTableView.delegate = self;
    [self.settingAppTableView registerNib:[UINib nibWithNibName:@"SettingCell" bundle:nil] forCellReuseIdentifier:@"SettingCell"];
    self.settingAppTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    _titleArray = @[@"Mobile data and WiFi",
                    @"WiFi only",
                    @"Never"];
    _selectedArray = [[NSMutableArray alloc] init];
    for (int i = 0; i <_titleArray.count; i++) {
        [_selectedArray addObject:[NSNumber numberWithBool:NO]];
    }
    if (![[NSUserDefaults standardUserDefaults]  objectForKey:KEY_SETTINGVIDEOTYPE])
    {
        [[NSUserDefaults standardUserDefaults] setObject:@(0) forKey:KEY_SETTINGVIDEOTYPE];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UITableView DataSource
- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  [_titleArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SettingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingCell"];
    [cell resetCell];
    cell.nameLabel.text = _titleArray[indexPath.row];
    int index = [[[NSUserDefaults standardUserDefaults]  objectForKey:KEY_SETTINGVIDEOTYPE] intValue];
    [cell.checkBoxImg setHidden: !(index == indexPath.row)];
    return cell;
    
}
#pragma mark - UITableView Delegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return  45;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [_selectedArray removeAllObjects];
    for (int i = 0; i <_titleArray.count; i++) {
        [_selectedArray addObject:[NSNumber numberWithBool:NO]];
    }
    if ([[_selectedArray objectAtIndex:indexPath.row] boolValue] == YES) {
        [_selectedArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
    }
    else{
        [[NSUserDefaults standardUserDefaults] setObject:@(indexPath.row) forKey:KEY_SETTINGVIDEOTYPE];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [_selectedArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:YES]];
    }
    [tableView reloadData];
}
@end
