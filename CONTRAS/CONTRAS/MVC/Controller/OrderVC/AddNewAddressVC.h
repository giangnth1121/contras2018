//
//  AddNewAddressVC.h
//  SACH
//
//  Created by Giang Béo  on 1/5/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "BaseViewController.h"

@interface AddNewAddressVC : BaseViewController

@property (weak, nonatomic) IBOutlet UIButton *btnComplete;
@end
