//
//  Video.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModel.h"

typedef enum : NSUInteger
{
    VideoGIF = 3,
    VideoTypeMp4 = 2,
    VideoTypeYt = 4,
    VideoTypeAdmob = 10,
    VideoTypeFB = 11
} VideoType;

@interface VideoModel : BaseModel
@property (nonatomic) NSInteger videoId;
@property (nonatomic) VideoType type;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *content;
@property (nonatomic, strong) NSString *img;
@property (nonatomic, strong) NSString *clip;
@property (nonatomic, strong) NSString *author;
@property (nonatomic, strong) NSString *avarta;
@property (nonatomic) NSInteger authorId;
@property (nonatomic) NSInteger like;
@property (nonatomic) NSInteger dislike;
@property (nonatomic) NSInteger comment;
@property (nonatomic) NSInteger share;
@property (nonatomic) NSInteger view;
@property (nonatomic) BOOL liked;
@property (nonatomic) BOOL disliked;
@property (nonatomic) BOOL shared;
@property (nonatomic) BOOL saved;
@property (nonatomic, strong) NSString *shareLink;
@property (nonatomic) NSInteger duration;

@property (nonatomic) NSInteger seekTime;
@property (nonatomic,strong) NSString *directLink;
@property (nonatomic,strong) NSString *adUUID;
@end

