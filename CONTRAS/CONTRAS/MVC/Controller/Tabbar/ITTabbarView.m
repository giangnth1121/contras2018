//
//  ITTabbarView.m
//  SpecTee
//
//  Created by Nguyen Ha Giang on 3/14/17.
//  Copyright © 2017 ITLancerApp. All rights reserved.
//

#import "ITTabbarView.h"
#import "UIImage+Utils.h"

/** Get image by name*/
#define Image(x)  [UIImage imageNamed:x]
#define MAIN_COLOR UIColorFromRGB(0x0cb8b6)
#define MAIN_BGCOLOR UIColorFromRGB(0xF9F9F9)
@interface ITTabbarView()

@property (weak, nonatomic) IBOutlet UILabel *numOfVideoLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imvNew;
@property (weak, nonatomic) IBOutlet UIImageView *imvStore;
@property (weak, nonatomic) IBOutlet UIImageView *imvTeam;
@property (weak, nonatomic) IBOutlet UIImageView *imvSetting;

@property (weak, nonatomic) IBOutlet UIImageView *imvBGNew;
@property (weak, nonatomic) IBOutlet UIImageView *imvBGStore;
@property (weak, nonatomic) IBOutlet UIImageView *imvBGTeam;
@property (weak, nonatomic) IBOutlet UIImageView *imvBGSetting;

@property (weak, nonatomic) IBOutlet UILabel *newsLabel;
@property (weak, nonatomic) IBOutlet UILabel *storeLabel;
@property (weak, nonatomic) IBOutlet UILabel *teamLabel;
@property (weak, nonatomic) IBOutlet UILabel *settingLabel;

@end
@implementation ITTabbarView{
    NSArray *_arrImage;
    NSArray *_arrImageSelected;
    NSArray *_arrTitle;
    NSArray *_arrTitleSelected;
    NSArray *_arrLabel;
    NSArray *_arrView;
    NSArray *_arrBgView;
}

#pragma mark - life cycle
-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    [self setupXib];
    
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    [self setupXib];
    
    return self;
}

-(void) setupXib {
    
    NSArray *nibs = [[NSBundle bundleForClass:self.class] loadNibNamed:@"ITTabbarView" owner:self options:nil];
    _view = [nibs objectAtIndex:0];
    _view.frame = self.bounds;
    _view.autoresizingMask = UIViewAutoresizingFlexibleWidth| UIViewAutoresizingFlexibleHeight;
    [self addSubview:_view];
    
    _arrImage           = @[@"ic_tb_new",
                            @"ic_tb_shop",
                            @"ic_tb_team",
                            @"ic_tb_setting"];
    _arrImageSelected   = @[@"ic_tb_new",
                            @"ic_tb_shop",
                            @"ic_tb_team",
                            @"ic_tb_setting"];
    _arrTitle           = @[@"Tin tức",
                            @"Contras Store",
                            @"Kết Nối",
                            @"Cài Đặt"];
    _arrTitleSelected   = @[@"Tin tức",
                            @"Contras Store",
                            @"Kết Nối",
                            @"Cài Đặt"];
    
    _arrView            = @[_imvNew, _imvStore, _imvTeam,_imvSetting];
    _arrBgView          = @[_imvBGNew, _imvBGStore, _imvBGTeam,_imvBGSetting];
    _arrLabel           = @[_newsLabel, _storeLabel,_teamLabel,_settingLabel];
    _selectedTabBarItemIndex = TabHome;
    [self chooseTab:TabHome];
}

- (void)setSelectedTabBarItemIndex:(TabName)selectedTabBarItemIndex{
   
    [self chooseTab:selectedTabBarItemIndex];
}

- (void) selectedTab:(NSUInteger)tabSelected{
    
    UIImageView *imv = _arrView[tabSelected];
    imv.image =  Image(_arrImageSelected[tabSelected]);//[Image(_arrImageSelected[tabSelected]) imageMaskedWithColor:[UIColor colorWithHex:@"FE585A" alpha:1.0f]];
    
    UILabel *lbl = _arrLabel[tabSelected];
    lbl.text = _arrTitleSelected[tabSelected];
    
}

- (void) deselectedTab:(NSUInteger)tabDeselected{
    
    [self.numOfVideoLabel setHidden:YES];
    UIImageView *imv = _arrView[tabDeselected];
    imv.image = Image(_arrImage[tabDeselected]);// [Image(_arrImage[tabDeselected]) imageMaskedWithColor:[UIColor colorWithHex:@"79C70D" alpha:1.0f]];
    UILabel *lbl = _arrLabel[tabDeselected];
    lbl.text = _arrTitle[tabDeselected];
}

- (void) chooseTab:(NSUInteger)tab{
   // dispatch_async(dispatch_get_main_queue(), ^{
        [self deselectedTab:_selectedTabBarItemIndex];
         _selectedTabBarItemIndex = tab;
        [self selectedTab:tab];
   // });
}
- (void)setNumOfNewUpdate:(NSInteger)num {
    [self.numOfVideoLabel setHidden: (num == 0)];
    self.numOfVideoLabel.layer.cornerRadius = self.numOfVideoLabel.frame.size.width/2;
    self.numOfVideoLabel.layer.masksToBounds = YES;
    self.numOfVideoLabel.text = (num >=100) ? @"99+" : [NSString stringWithFormat:@"%d",num];
}
#pragma mark - Action

- (IBAction)didTouchNewTapped:(id)sender {
    if (self.delegate) {
        [self.delegate tabBar:self didSelectItemAtIndex:0];
    }
    
}
- (IBAction)didTouchStoreTab:(id)sender {
    if (self.delegate) {
        [self.delegate tabBar:self didSelectItemAtIndex:1];
    }
}

- (IBAction)didTouchTeamTab:(id)sender {
    if (self.delegate) {
        [self.delegate tabBar:self didSelectItemAtIndex:2];
    }
}
- (IBAction)didTouchSettingTab:(id)sender {
    if (self.delegate) {
        [self.delegate tabBar:self didSelectItemAtIndex:3];
    }
}
@end
