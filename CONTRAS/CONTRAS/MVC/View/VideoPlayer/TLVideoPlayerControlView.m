//
//  TLVideoPlayerControlView.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "TLVideoPlayerControlView.h"
@implementation TLVideoPlayerControlView

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.fbButton.layer.cornerRadius = self.fbButton.frame.size.width/2;
    self.twButton.layer.cornerRadius = self.fbButton.frame.size.width/2;
    self.messengerButton.layer.cornerRadius = self.messengerButton.frame.size.width/2;
    self.moreButton.layer.cornerRadius = self.fbButton.frame.size.width/2;
}

-(void)setControlState:(ControlState)controlState
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissControlWhenPlaying) object:nil];
    _controlState = controlState;
    switch (controlState)
    {
        case ControlStateIdle:
            [self resetState];
            break;
            
        case ControlStateLoading:case ControlStateLoadingBuffer:
        {
            self.playButtonContainerView.alpha = 1;
            self.waitActivityView.hidden = NO;
            self.playButton.hidden = YES;
            [self.waitActivityView startAnimating];
            self.progressView.alpha = 0;
        }
            break;
        case ControlStatePlaying:
        {
            self.thumbnailImageView.hidden = YES;
            self.playButton.selected = YES;
            [self.waitActivityView stopAnimating];
            
            [UIView animateWithDuration:0.2 animations:^{
                self.videoTitleLabel.alpha = 0;
                self.videoTimeContainerView.alpha = 0;
                self.playButtonContainerView.alpha = 0;
                self.seekControlContainerView.alpha = 0;
                self.progressView.alpha = 1;
            }];
        }
            break;
        case ControlStateShowSeekControl:case ControlStatePause:
            self.thumbnailImageView.hidden = YES;
            self.videoTitleLabel.hidden = NO;
            self.seekControlContainerView.hidden = NO;
            self.playButton.hidden = NO;
            [self.waitActivityView stopAnimating];
            
            [UIView animateWithDuration:0.2 animations:^{
                self.videoTitleLabel.alpha = 1;
                self.videoTimeContainerView.alpha = 0;
                self.playButtonContainerView.alpha = 1;
                self.seekControlContainerView.alpha = 1;
                self.progressView.alpha = 0;
            }];
            
            if (controlState == ControlStateShowSeekControl)
            {
                [self performSelector:@selector(dismissControlWhenPlaying) withObject:nil afterDelay:3];
            }
            else
            {
                self.playButton.selected = NO;
            }
            
            break;
    }
}

-(void)setVideoTitle:(NSString *)videoTitle
{
    _videoTitle = videoTitle;
    
    self.videoTitleLabel.text = videoTitle;
}

-(void)setVideoDuration:(NSInteger)videoDuration
{
    _videoDuration = videoDuration;

    NSString *duration = [Utils getTimeStringFromTime:videoDuration];
    self.videoTimeLabel.text = duration;
}

-(void)setThumbnailURL:(NSString *)thumbnailURL
{
    _thumbnailURL = thumbnailURL;
    
    [self.thumbnailImageView sd_setImageWithURL:[NSURL URLWithString:thumbnailURL]];
}

-(void)resetState
{
    self.videoTitleLabel.alpha = 1;
    self.videoTimeContainerView.alpha = 1;
    
    self.playButton.hidden = NO;
    self.playButton.selected = NO;
    self.playButtonContainerView.alpha = 1;
    
    [self.waitActivityView stopAnimating];
    self.thumbnailImageView.hidden = NO;
    self.progressView.alpha = 0;
    self.progressView.progress = 0;
    
    [self.waitActivityView stopAnimating];
    self.waitActivityView.hidden = YES;
    
    self.seekControlContainerView.hidden = YES;
    _controlState = ControlStateIdle;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissControlWhenPlaying) object:nil];
}

- (IBAction)playButtonDidTap:(UIButton *)sender
{
    sender.selected = !sender.selected;
    
    if (sender.selected)
    {
        [self.delegate TLVideoPlayerControlViewDidTapPlay:self];
    }
    else
    {
        [self.delegate TLVideoPlayerControlViewDidTapPause:self];
    }
}

- (IBAction)fullscreenButtonDidTap:(id)sender
{
    if ([self.delegate respondsToSelector:@selector(TLVideoPlayerControlViewDidTapFullscreen:)])
    {
        [self.delegate TLVideoPlayerControlViewDidTapFullscreen:self];
    }
}

- (IBAction)sliderDidChangeValue:(UISlider *)sender
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(dismissControlWhenPlaying) object:nil];
    
    if ([self.delegate respondsToSelector:@selector(TLVideoPlayerControlView:didSeekToValue:)])
    {
        [self.delegate TLVideoPlayerControlView:self didSeekToValue:sender.value];
    }
    
    if (self.controlState == ControlStateShowSeekControl)
    {
        [self performSelector:@selector(dismissControlWhenPlaying) withObject:nil afterDelay:3];
    }
}

-(void) dismissControlWhenPlaying
{
    [self setControlState:ControlStatePlaying];
}
- (IBAction)shareFBTapped:(id)sender  {
     [self.delegate videoShareFb:self];
}
- (IBAction)shareTWTapped:(id)sender  {
    [self.delegate videoShareTw:self];
}
- (IBAction)shareMoreTapped:(id)sender  {
    [self.delegate videoShareMore:self];
}
- (IBAction)replayTapped:(id)sender  {
    [self.delegate videoReplay:self];
}
@end
