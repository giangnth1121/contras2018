//
//  UIColor+Ext.m
//  newwindsoft
//
//  Created by Vu Hoang Minh on 5/28/13.
//  Copyright (c) 2013 newwindsoft . All rights reserved.
//

#import "UIColor+Ext.h"

@implementation UIColor (Ext)

+ (UIColor *)colorWithHex:(NSString *)colorHex alpha:(CGFloat)alpha {
    
    unsigned int red, green, blue;
	NSRange range;
	range.length = 2;
	range.location = 0;
	[[NSScanner scannerWithString:[colorHex substringWithRange:range]] scanHexInt:&red];
	range.location = 2;
	[[NSScanner scannerWithString:[colorHex substringWithRange:range]] scanHexInt:&green];
	range.location = 4;
	[[NSScanner scannerWithString:[colorHex substringWithRange:range]] scanHexInt:&blue];
    
	return [UIColor colorWithRed:(float)(red/255.0f) green:(float)(green/255.0f) blue:(float)(blue/255.0f) alpha:alpha];
}


+ (UIColor *)kColorButtonShadowYellow{
    
    return  [UIColor colorWithRed:(float)(216/255.0f) green:(float)(123/255.0f) blue:(float)(3/255.0f) alpha:1.0f];
}
+ (UIColor *)kColorButtonShadowBlue{
    
    return  [UIColor colorWithRed:(float)(80/255.0f) green:(float)(127/255.0f) blue:(float)(200/255.0f) alpha:1.0f];
}
+ (UIColor *)kColorLineCell {
   return  [UIColor colorWithRed:(float)(204/255.0f) green:(float)(204/255.0f) blue:(float)(204/255.0f) alpha:1.0f];
}


+ (UIColor *)kColorDefaultBlue {
    return  [UIColor colorWithRed:(float)(90/255.0f) green:(float)(142/255.0f) blue:(float)(233/255.0f) alpha:1.0f];
}
+ (UIColor *)kColorDefaultAvatar {
 return  [UIColor colorWithRed:(float)(232/255.0f) green:(float)(232/255.0f) blue:(float)(232/255.0f) alpha:1.0f];
}
+ (UIColor *)kBackgroundColorGray {
    return  [UIColor colorWithHex:@"F4F4F4" alpha:1.0f];;
}
+ (UIColor *)kBackgroundColorBlue {
    return  [UIColor colorWithHex:@"E4EDEF" alpha:1.0f];
}
+ (UIColor*)warmGrey {
    return [UIColor colorWithRed:151./255. green:151./255. blue:151./255. alpha:1.0];
}

+ (UIColor*)kColorBackgroundDisableButton {
    return [UIColor colorWithRed:153./255. green:153./255. blue:153./255. alpha:1.0];
}

+ (UIColor*)kColorButtonShadowGray {
    return [UIColor colorWithRed:128./255. green:128./255. blue:128./255. alpha:1.0];
}

+ (UIColor*)buttonTitleColorBlue {
    return [UIColor colorWithRed:21./255. green:126./255. blue:251./255. alpha:1.0];
}

@end

