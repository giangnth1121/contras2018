//
//  SWCollectionCell.h
//  SRPLS
//
//  Created by Tan Le on 7/26/14.
//  Copyright (c) 2014 Tan Le. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SWCollectionViewCell : UICollectionViewCell
{
    
}

// property for cell colection
@property (nonatomic,assign) NSIndexPath *indexPath;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,strong) UILabel *nameUserLabel;
@property (nonatomic,strong) UILabel *priceLabel;
@property (nonatomic,strong) UILabel *oldPriceLabel;
@property (nonatomic,strong) UILabel *detailLabel;
@property (nonatomic,strong) UILabel *numberCommentLabel;
@property (nonatomic,strong) UILabel *numberFavouriteLabel;

- (void) displayCommentNumber:(BOOL) flag;

@end
