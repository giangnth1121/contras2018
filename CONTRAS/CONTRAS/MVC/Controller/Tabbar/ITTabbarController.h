//
//  ITTabbarController.h
//  SpecTee
//
//  Created by Nguyen Ha Giang on 3/14/17.
//  Copyright © 2017 ITLancerApp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ITTabbarController : UITabBarController
{}

- (void)hideTabbar:(BOOL)isHide ;
- (void)setNumOfNewUpdate:(NSInteger)num;
@end
