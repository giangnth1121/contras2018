//
//  HomeCollectionViewCell.h
//  SACH
//
//  Created by Giang Béo  on 12/25/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImg;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
- (void)setContentCell:(NSDictionary *)dict;
@end
