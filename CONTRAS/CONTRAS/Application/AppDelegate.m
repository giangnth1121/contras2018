//
//  AppDelegate.m
//  SACH
//
//  Created by Giang Béo  on 12/24/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeVC.h"
#import <Google/SignIn.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import TwitterKit;
@import Firebase;
#define TWITTER_SCHEME @"twitterkit-ogxkkwsae7awuv9pyu4e1o8ro"
#define FACEBOOK_SCHEME  @"fb127876981251005"

@interface AppDelegate () <FIRMessagingDelegate>
{
    HomeVC *_homeVC;
}
@end

@implementation AppDelegate

+ (AppDelegate *)shareAppDelegate{
    return (AppDelegate *)[UIApplication sharedApplication].delegate;
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [self registerPushNotification];
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    [[Twitter sharedInstance] startWithConsumerKey:@"OgXKkWsae7awuv9PyU4E1O8Ro" consumerSecret:@"FmGXL744pqixeUI9tTVGs9YU45riZx27RPA8qycPPVPbJlQ85h"];
    
    [Fabric with:@[[Crashlytics class]]];
    [FIRApp configure];
    [FIRMessaging messaging].remoteMessageDelegate = self;
    
    [self initTabbarController];
     [self.window makeKeyAndVisible];
    return YES;
}
- (void) registerPushNotification {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(isRegisteredForRemoteNotifications)]){
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
#if __IPHONE_OS_VERSION_MIN_REQUIRED < 80000
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)];
#endif
    }
}
- (void) initTabbarController{
    self.window.rootViewController = nil;
    
    self.tabbarController = [[ITTabbarController alloc] init];
    self.window.rootViewController = self.tabbarController;
}

- (void)hideTabbar:(BOOL)isHide {
    
    [self.tabbarController hideTabbar:isHide];
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    
    if ([[url scheme] isEqualToString:FACEBOOK_SCHEME])
        return [[FBSDKApplicationDelegate sharedInstance] application:application
                                                              openURL:url
                                                    sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                                           annotation:nil];
    if ([[url scheme] isEqualToString:TWITTER_SCHEME])
        return [[Twitter sharedInstance] application:application openURL:url options:options];
    
    return [[GIDSignIn sharedInstance] handleURL:url
                               sourceApplication:options[UIApplicationOpenURLOptionsSourceApplicationKey]
                                      annotation:nil];
    //    return NO;
}

@end
