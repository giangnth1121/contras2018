//
//  ListConfirmOrderVC.m
//  SACH
//
//  Created by Giang Béo  on 12/29/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "ListConfirmOrderVC.h"
#import "OrderSubmitCell.h"
#import "ListAddressVC.h"

@interface ListConfirmOrderVC () <UITableViewDataSource, UITableViewDelegate,OrderSubmitCellDelegate>
{
    NSMutableArray *_orders;
    BOOL _isCheck;
    NSInteger _totalPrice;
}

@end

@implementation ListConfirmOrderVC
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:YES];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    self.title = @"DANH SÁCH ĐƠN HÀNG";
    [self setBackNavigationButton];
    self.orderTableView.dataSource = self;
    self.orderTableView.delegate = self;
    [self.orderTableView registerNib:[UINib nibWithNibName:@"OrderSubmitCell" bundle:nil] forCellReuseIdentifier:@"OrderSubmitCell"];
    [self setBackNavigationButton];
    self.buyButton.layer.cornerRadius = 5.0f;
    self.buyButton.backgroundColor = [UIColor purpleColor];
    
    self.totalPriceLabel.font = [UIFont boldSystemFontOfSize:17];
    self.totalPriceLabel.textColor = [UIColor purpleColor];
    _orders = [[NSMutableArray alloc] init];
    for (int i = 0; i < [self.ordersArray count]; i++) {
        NSDictionary *dict = self.ordersArray[i];
        if ([dict[@"number"] integerValue] != 0) {
            [_orders addObject:dict];
        }
    }
    _totalPrice = 0;
    [self getTotalPrice];
    [self.orderTableView reloadData];
    _isCheck = YES;

}

- (void)getTotalPrice {
    _totalPrice = 0;
    for (int i = 0; i < [_orders count]; i ++) {
        NSDictionary *dict = _orders[i];
        _totalPrice += [dict[@"price"] integerValue]* [dict[@"number"] integerValue] ;
    }
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%@",[Utils formatCurrency:_totalPrice] ];
    self.totalCoinsLabel.text = [NSString stringWithFormat:@"Nhận %ld xu",_totalPrice/30000];
    
    self.shipLabel.text =  (_totalPrice > 250000) ? @"Free ship (Miễn phí vận chuyển với hoá đơn > 250k)" : @"30.000đ (Miễn phí vận chuyển với hoá đơn > 250k)";
    [self checkStatusOrder];
}
- (void)checkStatusOrder {
    self.buyButton.userInteractionEnabled   = _totalPrice > 0;
    self.buyButton.alpha                    = (_totalPrice > 0) ? 1.0f : 0.5f;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - IBAction
- (IBAction)buyButtonTapped:(id)sender {
    ListAddressVC *addressVC = [[ListAddressVC alloc] initWithNibName:@"ListAddressVC" bundle:nil];
    addressVC.ordersArray = _orders;
    [self.navigationController pushViewController:addressVC animated:YES];
}
- (IBAction)selectFruitTapped:(UIButton *)sender {
    sender.selected = !sender.selected;
    _isCheck = !_isCheck;
    if (_isCheck) {
        [self getTotalPrice];
    } else {
        _totalPrice = 0;
        self.totalPriceLabel.text = @"0đ";
        self.totalCoinsLabel.text = @"";
    }
    [self checkStatusOrder];
    [self.orderTableView reloadData];
}

#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [_orders count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderSubmitCell *orderCell = [tableView dequeueReusableCellWithIdentifier:@"OrderSubmitCell" forIndexPath:indexPath];
    orderCell.delegate = self;
    NSDictionary *dict = _orders [indexPath.row];;
    [orderCell setContentOrderCell:dict];
    [orderCell setSelectedButton:_isCheck];
    return orderCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 80;
}
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //remove the deleted object from your data source.
        //If your data source is an NSMutableArray, do this
        [_orders removeObjectAtIndex:indexPath.row];
        [tableView reloadData]; // tell table to refresh now
    }
}
#pragma mark - OrderCellDelegate
- (void)checkRemoveOrder:(NSDictionary *)info isRemove:(BOOL)isRemove idx:(NSInteger)idx {
    
    if (isRemove) {
        [_orders removeObject:info];
    } else {
        [_orders addObject:info];
    }
    [self getTotalPrice];
}
- (void)updateOrderInfo:(NSDictionary*)info idx:(NSInteger)idx{
    OrderSubmitCell *cell = [_orderTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:idx inSection:0]];
    for (int i =0 ;i < [_orders count]; i++) {
        NSDictionary *dict = _orders[i];
        if ([info[@"order"] integerValue] == [dict[@"order"] integerValue] && cell.checkButton.selected) {
            
            [_orders removeObject:dict];
            [_orders addObject:info];
        }
    }
    [self getTotalPrice];
}
@end
