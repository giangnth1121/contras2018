//
//  PaymentCell.h
//  CONTRAS
//
//  Created by Giang Béo  on 3/1/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentCell : UITableViewCell
@property (nonatomic, weak) IBOutlet UIImageView *imgView;
@property (nonatomic, weak) IBOutlet UIImageView *imgCheck;
@property (nonatomic, weak) IBOutlet UILabel *lbTitle;
@end
