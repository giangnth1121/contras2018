//
//  TLVideoPlayerView.h
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "TLVideoPlayerControlView.h"
typedef enum : NSUInteger {
    TLVideoPlayerStatusNotPlay = 0,
    TLVideoPlayerStatusPlaying = 1,
    TLVideoPlayerStatusPause = 2
} TLVideoPlayerStatus;

@protocol TLVideoPlayerDelegate;

@interface TLVideoPlayerView : UIView
@property (weak, nonatomic) id<TLVideoPlayerDelegate> delegate;
@property (nonatomic,strong) VideoModel *video;

@property (nonatomic,strong) AVPlayer *player;
@property (nonatomic,strong) TLVideoPlayerControlView *controlView;
@property (nonatomic) TLVideoPlayerStatus status;
@property (nonatomic) UIViewController * videoController;
-(void) play;
-(void) pause;
-(void) stop;
-(void) stopHereOnly;
-(void) seekTimeTo:(NSTimeInterval)time;
@end


@protocol TLVideoPlayerDelegate <NSObject>
@optional
-(void) videoPlayerDidTapPlay:(TLVideoPlayerView *)player;
-(void) videoPlayerDidTapFullScreen:(TLVideoPlayerView *)player;
-(void) videoPlayer:(TLVideoPlayerView *)player didChangePlayerItemStatus:(AVPlayerItemStatus)status;
-(void) videoPlayer:(TLVideoPlayerView *)player didChangeCurrentTime:(NSTimeInterval)currentTime durationTime:(NSTimeInterval)videoDuration;
-(void) videoPlayerDidEndVideo:(TLVideoPlayerView *)player;
@end
