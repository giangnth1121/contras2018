//
//  UIImage+Extensions.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "UIImage+Extensions.h"

@implementation UIImage (Extensions)
-(NSString *)base64
{
    NSData *data = UIImagePNGRepresentation(self);
    return [data base64EncodedStringWithOptions:0];
}

-(instancetype)initWithBase64String:(NSString *)base64String
{
    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:base64String options:0];
    
    if (decodedData)
    {
        return [UIImage imageWithData:decodedData];
    }
    
    return nil;
}

+(UIImage *)imageWithBase64String:(NSString *)base64String
{
    return [[UIImage alloc] initWithBase64String:base64String];
}
@end
