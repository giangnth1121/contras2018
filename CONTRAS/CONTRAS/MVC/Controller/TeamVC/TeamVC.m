//
//  TeamVC.m
//  CONTRAS
//
//  Created by Giang Béo  on 2/28/18.
//  Copyright © 2018 GiangNH. All rights reserved.
//

#import "TeamVC.h"
#import "SocialCell.h"

@interface TeamVC ()
{
    NSArray *_titles;
    NSArray *_images;
    NSArray *_links;
}
@end

@implementation TeamVC

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate shareAppDelegate] hideTabbar:NO];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self configUI];
    [self initData];
    // Do any additional setup after loading the view from its nib.
}
- (void)configUI {
    
    self.title = @"KẾT NỐI";
    _socialTableView.dataSource = self;
    _socialTableView.delegate = self;
    _socialTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_socialTableView registerNib:[UINib nibWithNibName:@"SocialCell" bundle:nil] forCellReuseIdentifier:@"SocialCell"];
}

- (void)initData {
    _titles = @[@"https://www.facebook.com/contrasstore/",
                @"https://plus.google.com/u/1/+hoicdvhanoitt",
                @"https://www.instagram.com/contrashanoi/",
                @"https://www.youtube.com/c/hoicdvhanoitt"];
    _images = @[@"ic_connect_fb",
                @"ic_connect_gg",
                @"ic_connect_instagram",
                @"ic_connect_yt"];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return  1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [_titles count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SocialCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SocialCell" forIndexPath:indexPath];
    cell.lbTitle.text = _titles[indexPath.row];
    cell.imgView.image = [UIImage imageNamed:_images[indexPath.row]];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSURL *url = [NSURL URLWithString:_titles[indexPath.row]];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        [[UIApplication sharedApplication] openURL:url options:NULL completionHandler:^(BOOL success) {
            
        }];
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

@end

