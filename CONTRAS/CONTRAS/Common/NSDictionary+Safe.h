//
//  NSDictionary+Safe.h
//  TGS
//
//  Created by Nguyen Ha Giang on 2/17/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Safe)
-(id) objForKey:(id)key;
@end

@interface NSDictionary (Debug)
-(NSString *) toJsonString;
@end
