//
//  RoundRectImageView.h
//  Assignment
//
//  Created by Nguyen Ha Giang on 9/15/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import <UIKit/UIKit.h>
IB_DESIGNABLE
@interface RoundRectImageView : UIImageView
@property (nonatomic) IBInspectable CGFloat cornerRadius;
@property (nonatomic) IBInspectable CGFloat borderWidth;
@property (nonatomic,strong) IBInspectable UIColor * borderColor;
@end
