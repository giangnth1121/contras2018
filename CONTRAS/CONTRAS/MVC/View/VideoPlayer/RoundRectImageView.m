//
//  RoundRectImageView.m
//  Assignment
//
//  Created by Nguyen Ha Giang on 9/15/17.
//  Copyright © 2017 OMI. All rights reserved.
//

#import "RoundRectImageView.h"

@implementation RoundRectImageView

-(void)setCornerRadius:(CGFloat)cornerRadius
{
    _cornerRadius = cornerRadius;
    self.layer.cornerRadius = cornerRadius;
    self.layer.masksToBounds = YES;
}

-(void)setBorderColor:(UIColor *)borderColor
{
    _borderColor = borderColor;
    self.layer.borderColor = borderColor.CGColor;
}

-(void)setBorderWidth:(CGFloat)borderWidth
{
    _borderWidth = borderWidth;
    self.layer.borderWidth = borderWidth;
}

@end
