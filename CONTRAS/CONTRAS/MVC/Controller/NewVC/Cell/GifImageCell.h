//
//  GiftImageCell.h
//  FunnyVideos
//
//  Created by Giang Béo  on 11/29/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FLAnimatedImage.h"

@protocol GifImageCellDelegate;
@interface GifImageCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *gifContainerView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImg;
@property (weak, nonatomic) IBOutlet UIImageView *videoPlaceHolderImageView;
@property (weak, nonatomic) IBOutlet FLAnimatedImageView *GIFImgView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *GIFLabel;
@property (weak, nonatomic) IBOutlet UIButton *numOfCommentButton;
@property (weak, nonatomic) IBOutlet UIButton *likeButton;
@property (weak, nonatomic) IBOutlet UIButton *dislikeButton;
@property (weak, nonatomic) IBOutlet UIButton *numOfShareButton;
@property (nonatomic )VideoModel *video;
@property (weak, nonatomic) id<GifImageCellDelegate> delegate;
@property (nonatomic) NSInteger idxRow;
- (void)setContentCell:(VideoModel *)video;
- (void)setContentCellBeforeAnimate:(VideoModel *)video ;
- (IBAction)loadGIFImageTapped:(id)sender;

-(void) stopPlayGif;
@end

@protocol GifImageCellDelegate <NSObject>
@optional
- (void)showLoginView;
- (void)commentVideoAtIndex:(NSInteger)idx;
- (void)shareVideoAtIndex:(NSInteger)idx;
-(void) GIFCellDidTapUnLikeUndislike:(GifImageCell *)cell;
-(void) GIFCellDidTapLike:(GifImageCell *)cell;
-(void) GIFCellDidTapDislike:(GifImageCell *)cell;
@end

