//
//  NSString+Util.m
//  TGS
//
//  Created by Nguyen Ha Giang on 2/20/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "NSString+Util.h"

@implementation NSString (Util)
-(NSURL *)toURL
{
    return [NSURL URLWithString:self];
}

-(NSAttributedString *)strikeAttributed
{
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:self];
//    [attributeString addAttribute:NSStrikethroughStyleAttributeName
//                            value:@1
//                            range:NSMakeRange(0, [attributeString length])];
    
    return attributeString;
}

-(NSArray<NSTextCheckingResult *> *)matchWithPattern:(NSString *)pattern
{
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:nil];
    
    return [regex matchesInString:self options:NSMatchingReportCompletion range:NSMakeRange(0, self.length)];
}

-(BOOL)checkWithPattern:(NSString *)pattern
{
    return [self matchWithPattern:pattern].count;
}
@end
