//
//  UserModel.m
//  FunnyVideos
//
//  Created by Giang Béo  on 11/13/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "UserModel.h"

@implementation UserModel
+(instancetype)parseWithDictionary:(NSDictionary *)dict
{
    //"id": 10,
    //"login_type": 2,
    //"username": "g",
    //"psw": null,
    //"email": "hagiangnguyen.iosdev@gmail.com",
    //"avarta": null,
    //"fullname": null
    UserModel *obj  = [[UserModel alloc] init];
    obj.userID       = [[dict objForKey:@"id"] integerValue];
    obj.username     = [dict objForKey:@"username"] ;
    obj.email        = [dict objForKey:@"email"];
    obj.avatar       = [dict objForKey:@"avarta"];
    obj.fullName     = [dict objForKey:@"fullname"];
    obj.loginType    = (loginType)[[dict objForKey:@"login_type"] integerValue];
    return obj;
}

@end
