//
//  NotifyCell.m
//  TopShare
//
//  Created by Giang Béo  on 11/30/17.
//  Copyright © 2017 Retech. All rights reserved.
//

#import "NotifyCell.h"

@implementation NotifyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.notifyImg.layer.cornerRadius = self.notifyImg.frame.size.width/2;
    self.notifyImg.layer.masksToBounds = YES;
    
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
