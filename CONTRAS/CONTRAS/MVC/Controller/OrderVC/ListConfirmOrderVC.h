//
//  ListConfirmOrderVC.h
//  SACH
//
//  Created by Giang Béo  on 12/29/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListConfirmOrderVC : BaseViewController

@property (weak, nonatomic) IBOutlet UITableView *orderTableView;
@property (weak, nonatomic) IBOutlet UITextField *codeForSaleTextField;
@property (weak, nonatomic) IBOutlet UILabel *shipLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalCoinsLabel;
@property (weak, nonatomic) IBOutlet UIButton *buyButton;
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (nonatomic, strong)  NSMutableArray *ordersArray;


@end
