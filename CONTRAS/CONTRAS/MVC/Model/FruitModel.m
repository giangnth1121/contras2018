//
//  FruitModel.m
//  SACH
//
//  Created by Giang Béo  on 12/29/17.
//  Copyright © 2017 GiangNH. All rights reserved.
//

#import "FruitModel.h"

@implementation FruitModel
+(instancetype)parseWithDictionary:(NSDictionary *)dict
{
    FruitModel *obj      = [[FruitModel alloc] init];
    obj.fruitId          = [[dict objForKey:@"id"] integerValue];
    obj.price            = [[dict objForKey:@"price"] integerValue];
    obj.price            = [[dict objForKey:@"number"] integerValue];
    obj.title            = [dict objForKey:@"title"] ;
    obj.image            = [dict objForKey:@"image"] ;
    obj.value            = [dict objForKey:@"value"] ;
    return obj;
}
@end
