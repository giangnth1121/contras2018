//
//  Video.m
//  TopShare
//
//  Created by Nguyen Ha Giang on 11/12/17.
//  Copyright © 2017 Nguyen Ha Giang. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel
-(instancetype)init
{
    self = [super init];
    
    self.adUUID = [[NSUUID alloc] init].UUIDString;
    return self;
}

+(instancetype)parseWithDictionary:(NSDictionary *)dict
{
    
    VideoModel *obj = [[VideoModel alloc] init];
    obj.videoId = [[dict objForKey:@"id"] integerValue];
    obj.type = [[dict objForKey:@"type"] integerValue];
    obj.title = [dict objForKey:@"title"];
    obj.content = [dict objForKey:@"content"];
    obj.img = [dict objForKey:@"img"];
    obj.clip = [dict objForKey:@"clip"];
    obj.avarta = [dict objForKey:@"avarta"];
    obj.author = [dict objForKey:@"author"];
    obj.authorId = [[dict objForKey:@"author_id"] integerValue];
    obj.like = [[dict objForKey:@"like"] integerValue];
    obj.dislike = [[dict objForKey:@"dislike"] integerValue];
    obj.comment = [[dict objForKey:@"comment"] integerValue];
    obj.share = [[dict objForKey:@"share"] integerValue];
    obj.shareLink = [dict objForKey:@"share_link"];
    obj.duration = [[dict objForKey:@"duration"] integerValue];
    obj.liked = [[dict objectForKey:@"liked"] boolValue];
    obj.saved = [[dict objectForKey:@"saved"] boolValue];
    obj.disliked = [[dict objectForKey:@"disliked"] boolValue];
    obj.shared = [[dict objectForKey:@"shared"] boolValue];
    obj.view =  [[dict objForKey:@"view"] integerValue];
    //    obj.type = VideoTypeYt;
    //    obj.clip = @"https://www.youtube.com/watch?v=-nnWBhKZeg0";
    if (obj.type == VideoTypeMp4)
    {
        obj.directLink = obj.clip;
    }
    
    return obj;
}

-(void)setSeekTime:(NSInteger)seekTime
{
    _seekTime = seekTime;
}
@end

